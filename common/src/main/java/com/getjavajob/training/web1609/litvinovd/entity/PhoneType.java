package com.getjavajob.training.web1609.litvinovd.entity;

/**
 * Created by ds.litvinov on 20.11.2016.
 */
public enum PhoneType {
    HOME, WORK, MOBILE
}
