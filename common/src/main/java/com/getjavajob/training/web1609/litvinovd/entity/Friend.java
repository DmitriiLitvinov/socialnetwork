package com.getjavajob.training.web1609.litvinovd.entity;

import javax.persistence.*;

/**
 * Created by ds.litvinov on 21.02.2017.
 */
@Entity
@Table(name = "Friends")
public class Friend {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "who")
    private Account who;
    @ManyToOne
    @JoinColumn(name = "withWhom")
    private Account withWhom;
    @ManyToOne
    @JoinColumn(name = "confirm")
    private Account confirm;
    @Enumerated(EnumType.STRING)
    private FriendStatus friendStatus;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Account getWho() {
        return who;
    }

    public void setWho(Account who) {
        this.who = who;
    }

    public Account getWithWhom() {
        return withWhom;
    }

    public void setWithWhom(Account withWhom) {
        this.withWhom = withWhom;
    }

    public Account getConfirm() {
        return confirm;
    }

    public void setConfirm(Account confirm) {
        this.confirm = confirm;
    }

    public FriendStatus getFriendStatus() {
        return friendStatus;
    }

    public void setFriendStatus(FriendStatus friendStatus) {
        this.friendStatus = friendStatus;
    }

    @Override
    public String toString() {
        return "Friend{" +
                "id=" + id +
                ", who=" + who +
                ", withWhom=" + withWhom +
                ", confirm=" + confirm +
                ", friendStatus=" + friendStatus +
                '}';
    }
}
