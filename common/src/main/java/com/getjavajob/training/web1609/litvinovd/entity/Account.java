package com.getjavajob.training.web1609.litvinovd.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by ds.litvinov on 17.11.2016.
 */

@Entity
@Table(name = "account")
public class Account implements IdManipulation, Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String surname;
    @Column(name = "middle_name")
    private String middleName;
    @Column(name = "birth_date")
    private GregorianCalendar birthDate;
    @Column(name = "home_address")
    private String homeAddress;
    @Column(name = "work_address")
    private String workAddress;
    private String email;
    private String icq;
    private String skype;
    @Column(name = "extra_info")
    private String extraInfo;
    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Phone> phones;
    @OneToMany(mappedBy = "who")
    private List<Friend> whoFriends;
    @OneToMany(mappedBy = "withWhom")
    private List<Friend> withWhom;
    @OneToMany(mappedBy = "confirm")
    private List<Friend> confirm;
    @OneToMany(mappedBy = "account")
    private List<AccountAndGroup> accountAndGroups;
    @OneToMany(mappedBy = "founder", cascade = CascadeType.REMOVE)
    private List<Group> groups;
    @OneToMany(mappedBy = "from")
    private List<Message> fromMessages;
    @OneToMany(mappedBy = "receiver")
    private List<Message> receiverMessages;
    @OneToMany(mappedBy = "sender")
    private List<WallMessage> wallSenderMessages;
    @OneToMany(mappedBy = "receiver")
    private List<WallMessage> wallReceiverMessages;
    private String password;
    @Lob
    @Column(name = "picture")
    private byte[] bytePicture;

    public Account() {
    }

    public List<AccountAndGroup> getAccountAndGroups() {
        return accountAndGroups;
    }

    public void setAccountAndGroups(List<AccountAndGroup> accountAndGroups) {
        this.accountAndGroups = accountAndGroups;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public GregorianCalendar getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(GregorianCalendar birthDate) {
        this.birthDate = birthDate;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public byte[] getBytePicture() {
        return bytePicture;
    }

    public void setBytePicture(byte[] bytePicture) {
        this.bytePicture = bytePicture;
    }

    public List<Friend> getWhoFriends() {
        return whoFriends;
    }

    public void setWhoFriends(List<Friend> whoFriends) {
        this.whoFriends = whoFriends;
    }

    public List<Friend> getWithWhom() {
        return withWhom;
    }

    public void setWithWhom(List<Friend> withWhom) {
        this.withWhom = withWhom;
    }

    private String printBirthDate() {
        if (birthDate == null) {
            return "0000-00-00";
        }
        return new SimpleDateFormat("yyyy-MM-dd").format(birthDate.getTime());
    }

    public List<Friend> getConfirm() {
        return confirm;
    }

    public void setConfirm(List<Friend> confirm) {
        this.confirm = confirm;
    }

    public List<Message> getFromMessages() {
        return fromMessages;
    }

    public void setFromMessages(List<Message> fromMessages) {
        this.fromMessages = fromMessages;
    }

    public List<Message> getReceiverMessages() {
        return receiverMessages;
    }

    public void setReceiverMessages(List<Message> receiverMessages) {
        this.receiverMessages = receiverMessages;
    }

    public List<WallMessage> getWallSenderMessages() {
        return wallSenderMessages;
    }

    public void setWallSenderMessages(List<WallMessage> wallSenderMessages) {
        this.wallSenderMessages = wallSenderMessages;
    }

    public List<WallMessage> getWallReceiverMessages() {
        return wallReceiverMessages;
    }

    public void setWallReceiverMessages(List<WallMessage> wallReceiverMessages) {
        this.wallReceiverMessages = wallReceiverMessages;
    }

    @Override
    public String toString() {
        return "Account [id=" + id + ", name=" + name + ", surname=" + surname + ", middleName=" + middleName + ", birthDate="
                + printBirthDate() + ", homeAddress=" + homeAddress
                + ", workAddress=" + workAddress + ", email=" + email + ", icq=" + icq + ", skype=" + skype
                + ", extraInfo=" + extraInfo + " phones: " + phones + " password: " + password + " ]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Account)) return false;

        Account account = (Account) o;

        return email.equals(account.email);
    }

    @Override
    public int hashCode() {
        return email.hashCode();
    }
}