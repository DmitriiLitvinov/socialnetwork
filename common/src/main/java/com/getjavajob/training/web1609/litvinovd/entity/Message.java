package com.getjavajob.training.web1609.litvinovd.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by ds.litvinov on 23.02.2017.
 */
@Entity
public class Message implements IdManipulation, Comparable<Message> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private Date date;
    @ManyToOne
    @JoinColumn(name = "sender")
    private Account from;
    @ManyToOne
    @JoinColumn(name = "receiver")
    private Account receiver;
    private String text;

    public Message() {
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Account getFrom() {
        return from;
    }

    public void setFrom(Account from) {
        this.from = from;
    }

    public Account getReceiver() {
        return receiver;
    }

    public void setReceiver(Account receiver) {
        this.receiver = receiver;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", date=" + date +
                ", from=" + from +
                ", receiver=" + receiver +
                ", text='" + text + '\'' +
                '}';
    }

    @Override
    public int compareTo(Message o) {
        return date.compareTo(o.date);
    }
}
