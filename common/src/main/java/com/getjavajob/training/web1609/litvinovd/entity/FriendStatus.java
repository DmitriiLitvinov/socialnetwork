package com.getjavajob.training.web1609.litvinovd.entity;

/**
 * Created by ds.litvinov on 23.11.2016.
 */
public enum FriendStatus {
    PENDING, FRIEND, STRANGER
}
