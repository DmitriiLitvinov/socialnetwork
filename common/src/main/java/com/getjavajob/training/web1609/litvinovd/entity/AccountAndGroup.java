package com.getjavajob.training.web1609.litvinovd.entity;

import javax.persistence.*;

/**
 * Created by ds.litvinov on 02.02.2017.
 */
@Entity
@Table(name = "accountVSgroup")
public class AccountAndGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_group_id")
    private long id;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idAccount")
    private Account account;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idGroup")
    private Group group;
    @Enumerated(EnumType.STRING)
    private AccountGroupStatus status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public AccountGroupStatus getStatus() {
        return status;
    }

    public void setStatus(AccountGroupStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "AccountAndGroup{" +
                "id=" + id +
                ", account=" + account +
                ", group=" + group +
                ", status=" + status +
                '}';
    }
}
