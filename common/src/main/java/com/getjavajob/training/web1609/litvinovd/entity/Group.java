package com.getjavajob.training.web1609.litvinovd.entity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ds.litvinov on 19.11.2016.
 */
@Entity
@Table(name = "groups")
public class Group implements IdManipulation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "founder", referencedColumnName = "id")
    private Account founder;
    @OneToMany(mappedBy = "group")
    private List<AccountAndGroup> accountAndGroups;
    private String name;
    private String description;
    @Column(name = "picture", length = 10000)
    private byte[] bytePicture;
    @Transient
    private String stringPicture;

    public Group() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Account getFounder() {
        return founder;
    }

    public void setFounder(Account founder) {
        this.founder = founder;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getBytePicture() {
        return bytePicture;
    }

    public void setBytePicture(byte[] bytePicture) {
        this.bytePicture = bytePicture;
    }

    public List<AccountAndGroup> getAccountAndGroups() {
        return accountAndGroups;
    }

    public void setAccountAndGroups(List<AccountAndGroup> accountAndGroups) {
        this.accountAndGroups = accountAndGroups;
    }

    public String getStringPicture() {
        return stringPicture;
    }

    public void setStringPicture(String stringPicture) {
        this.stringPicture = stringPicture;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Group other = (Group) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", founder=" + founder +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' + ", listAccount: " +
                '}';
    }
}

