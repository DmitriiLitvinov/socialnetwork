package com.getjavajob.training.web1609.litvinovd.entity;

import javax.persistence.*;

/**
 * Created by ds.litvinov on 20.11.2016.
 */
@Entity
public class Phone implements IdManipulation {

    @Id
    private Long num;
    @Enumerated(EnumType.STRING)
    private PhoneType type;
    @ManyToOne
    @JoinColumn(name = "person", referencedColumnName = "id")
    private Account owner;

    public Phone() {
    }

    public Phone(PhoneType type, Long num) {
        this.type = type;
        this.num = num;
    }

    public Long getNum() {
        return num;
    }

    public void setNum(Long num) {
        this.num = num;
    }

    public PhoneType getType() {
        return type;
    }

    public void setType(PhoneType type) {
        this.type = type;
    }

    public Account getOwner() {
        return owner;
    }

    public void setOwner(Account owner) {
        this.owner = owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Phone phone = (Phone) o;

        return num.equals(phone.num);
    }

    @Override
    public int hashCode() {
        return num.hashCode();
    }

    @Override
    public Long getId() {
        return num;
    }

    @Override
    public void setId(Long id) {
        num = id;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "num=" + num +
                ", type=" + type +
                '}';
    }
}
