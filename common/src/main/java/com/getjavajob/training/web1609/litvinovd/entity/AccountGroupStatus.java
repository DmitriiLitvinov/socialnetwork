package com.getjavajob.training.web1609.litvinovd.entity;

/**
 * Created by ds.litvinov on 02.02.2017.
 */
public enum AccountGroupStatus {
    VIEWER, REQUEST, MEMBER
}
