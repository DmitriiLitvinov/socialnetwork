package com.getjavajob.training.web1609.litvinovd.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by ds.litvinov on 28.02.2017.
 */
@Entity
public class WallMessage implements Comparable<WallMessage> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private Date date;
    @ManyToOne
    @JoinColumn(name = "sender")
    private Account sender;
    @ManyToOne
    @JoinColumn(name = "receiver")
    private Account receiver;
    private String text;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Account getSender() {
        return sender;
    }

    public void setSender(Account sender) {
        this.sender = sender;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Account getReceiver() {
        return receiver;
    }

    public void setReceiver(Account receiver) {
        this.receiver = receiver;
    }

    @Override
    public String toString() {
        return "WallMessage{" +
                "id=" + id +
                ", date=" + date +
                ", sender.id=" + sender.getId() +
                ", text='" + text + '\'' +
                '}';
    }

    @Override
    public int compareTo(WallMessage o) {
        return o.date.compareTo(date);
    }
}
