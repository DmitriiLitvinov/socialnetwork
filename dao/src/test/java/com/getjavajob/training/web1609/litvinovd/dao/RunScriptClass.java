package com.getjavajob.training.web1609.litvinovd.dao;

import com.getjavajob.training.web1609.litvinovd.BDException.CRUDException;
import org.h2.tools.RunScript;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by ds.litvinov on 20.11.2016.
 */
public class RunScriptClass {
    public static void runScript(Connection connection, String... str) throws CRUDException {
        for (String fileName : str) {
            try {
                RunScript.execute(connection, new InputStreamReader(
                        RunScriptClass.class.getClassLoader().getResourceAsStream(fileName)));
            } catch (SQLException e) {
                throw new CRUDException(e.toString());
            }
        }
    }
}
