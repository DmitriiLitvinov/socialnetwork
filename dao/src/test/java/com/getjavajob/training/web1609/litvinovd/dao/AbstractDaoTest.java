package com.getjavajob.training.web1609.litvinovd.dao;

import com.getjavajob.training.web1609.litvinovd.BDException.CRUDException;
import com.getjavajob.training.web1609.litvinovd.entity.Account;
import com.getjavajob.training.web1609.litvinovd.entity.Group;
import com.getjavajob.training.web1609.litvinovd.entity.Phone;
import com.getjavajob.training.web1609.litvinovd.entity.PhoneType;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ds.litvinov on 21.11.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:dao-context-overrides.xml"})
public abstract class AbstractDaoTest {

    @Autowired
    protected AccountDao accountDao;
    protected Account ac;
    protected Connection connection;
    protected Group group;
    @Autowired
    protected GroupDao groupDao;
    protected Phone phone;
    @Autowired
    private DataSource dataSource;

    @Before
    public void init() throws CRUDException, InterruptedException {
        try {
            Class.forName("org.h2.Driver");
            connection = dataSource.getConnection();

            connection.setAutoCommit(true);
            RunScriptClass.runScript(connection,
                    "account/dropAccountTable.sql", "group/dropGroupTable.sql",
                    "phone/dropPhoneTable.sql", "accountVSgroup/dropAccountVSGroupTable.sql",
                    "friends/dropFriendsTable.sql");
            RunScriptClass.runScript(connection,
                    "account/createAccountTable.sql", "account/insertIntoAccount.sql",
                    "group/createGroupTable.sql", "group/insertIntoGroup.sql",
                    "phone/createPhoneTable.sql", "phone/insertIntoPhones.sql",
                    "accountVSgroup/createAccountVSGroupTable.sql", "accountVSgroup/insertIntoAccountVSGroup.sql",
                    "friends/createFriendsTable.sql", "friends/insertIntoFriends.sql");
        } catch (ClassNotFoundException | SQLException e) {
            throw new CRUDException(e.toString());
        }
    }

    @Before
    public void initializeAccount() {
        ac = new Account();
        ac.setName("Vova");
        ac.setSurname("Gazmanov");
        ac.setEmail("brightDays@mail.ru");
        List<Phone> list = new ArrayList<>();
        Phone phone1 = new Phone();
        phone1.setNum(2312312231L);
        phone1.setType(PhoneType.HOME);
        list.add(phone1);
        ac.setPhones(list);
        ac.setPassword("12431cdd23d23d23ed23d23");

    }

    @Before
    public void initializeGroup() throws CRUDException, InterruptedException {
        group = new Group();
        group.setName("IT-Sphere");
        group.setFounder(accountDao.get(1));
        group.setDescription("Be aware of the newest technology");
    }

    @Before
    public void initializePhone() {
        phone = new Phone();
        phone.setNum(123454321L);
        phone.setType(PhoneType.HOME);
    }

    @After
    public void dropAndClose() throws CRUDException {
        try {
            connection.close();
        } catch (SQLException e) {
            throw new CRUDException(e.toString());
        }
    }
}
