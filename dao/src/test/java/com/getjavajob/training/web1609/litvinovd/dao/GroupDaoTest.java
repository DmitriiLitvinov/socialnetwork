package com.getjavajob.training.web1609.litvinovd.dao;

import com.getjavajob.training.web1609.litvinovd.BDException.CRUDException;
import com.getjavajob.training.web1609.litvinovd.entity.Account;
import com.getjavajob.training.web1609.litvinovd.entity.Group;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by ds.litvinov on 19.11.2016.
 */
public class GroupDaoTest extends AbstractDaoTest {

    @Test
    @Transactional
    public void insertNewTest() {
        assertNull(group.getId());
        groupDao.insert(group);
        assertNotNull(group.getId());
    }

    @Test
    @Transactional
    public void insertExistingTest() {
        groupDao.insert(group);
        groupDao.insert(group);
    }

    @Test
    @Transactional
    public void deleteTest() throws CRUDException, InterruptedException {
        int id = 3;
        Group g = groupDao.get(id);
        assertNotNull(g);
        groupDao.delete(g);
        assertNull(groupDao.get(id));
    }

    @Test
    @Transactional
    public void getTest() {
        long id = groupDao.insert(group).getId();
        assertEquals(group.getName(), groupDao.get(id).getName());
    }

    @Test
    @Transactional
    public void updateTest() throws CRUDException, InterruptedException {
        int id = 1;
        String str = "awesome group";
        Group g = groupDao.get(id);
        assertNotEquals(str, g.getName());
        g.setName(str);
        groupDao.update(g);
        assertEquals(str, groupDao.get(id).getName());
    }

    @Test
    @Transactional
    public void updateFounderTest() throws CRUDException, InterruptedException {
        Group g = groupDao.get(1);
        Account ac = accountDao.get(3);
        g.setFounder(ac);
        groupDao.update(g);
        assertEquals(new Long(3), groupDao.get(1).getFounder().getId());
    }

    @Test
    @Transactional
    public void searchByNameTest() throws ClassNotFoundException, InterruptedException, CRUDException {
        Map<String, String> map = new HashMap<>();
        map.put("name", "carLovers");
        assertTrue(!groupDao.searchByName(map, 0, 10).isEmpty());
    }

    @Test
    @Transactional
    public void getAllTest() throws InterruptedException, CRUDException {
        assertFalse(groupDao.getAll().isEmpty());
    }

    @Test
    @Transactional
    public void getAccountsTest() throws InterruptedException, CRUDException {
        Group group = groupDao.get(1);
        assertEquals(2, groupDao.getAccounts(group).size());
    }
}
