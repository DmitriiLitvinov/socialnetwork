package com.getjavajob.training.web1609.litvinovd.dao;

import com.getjavajob.training.web1609.litvinovd.BDException.CRUDException;
import com.getjavajob.training.web1609.litvinovd.entity.Account;
import com.getjavajob.training.web1609.litvinovd.entity.Group;
import com.getjavajob.training.web1609.litvinovd.entity.Phone;
import com.getjavajob.training.web1609.litvinovd.entity.PhoneType;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by ds.litvinov on 17.11.2016.
 */
public class AccountDaoTest extends AbstractDaoTest {

    @Test
    @Transactional
    public void insertNewTest() {
        assertNull(ac.getId());
        accountDao.insert(ac);
        assertNotNull(ac.getId());
    }

    @Test
    @Transactional
    public void insertExistingTest() {
        accountDao.insert(ac);
        accountDao.insert(ac);
    }

    @Test
    @Transactional
    public void getTest() {
        long id = accountDao.insert(ac).getId();
        Account account = accountDao.get(id);
        assertEquals(account.getName(), accountDao.get(id).getName());
    }

    @Test
    @Transactional
    public void getAllTest() {
        assertEquals(5, accountDao.getAll().size());
    }

    @Test
    @Transactional
    public void deleteTest() {
        int id = 1;
        Account ac = accountDao.get(id);
        assertNotNull(ac);
        accountDao.delete(ac);
        assertNull(accountDao.get(id));
    }

    @Test
    @Transactional
    public void updateTest() throws CRUDException {
        int id = 1;
        Account ac = accountDao.get(id);
        String expected = "Timur";
        ac.setName(expected);
        accountDao.update(ac);
        assertEquals(expected, accountDao.get(id).getName());
    }

    @Test
    @Transactional
    public void searchByNameTest() {
        Map<String, String> map = new HashMap<>();
        map.put("name", "Alex");
        map.put("surname", "Alex");
        assertTrue(!accountDao.searchByName(map, 0, 10).isEmpty());
    }

    @Test
    @Transactional
    public void getGroupTest() throws CRUDException {
        Account account = accountDao.get(2);
        assertEquals(1, accountDao.getGroups(account).size());
    }

    @Test
    @Transactional
    public void getPhonesTest() {
        Account account = accountDao.get(1);
        assertEquals(2, account.getPhones().size());
    }

    @Test
    @Transactional
    public void updatePhoneTest() {
        Account account = accountDao.get(2);
        List<Phone> phoneList = account.getPhones();
        int size = phoneList.size();
        Phone phone = new Phone();
        phone.setOwner(account);
        phone.setNum(4815162342L);
        phone.setType(PhoneType.WORK);
        phoneList.add(phone);
        accountDao.update(account);
        assertEquals(size + 1, accountDao.get(account.getId()).getPhones().size());
    }

    @Test
    @Transactional
    public void joinGroupTest() {
        Account account = accountDao.get(5);
        Group group = groupDao.get(4);
        accountDao.joinGroup(account, group);
        assertEquals(account, accountDao.requestAccount(group).get(0));
    }

    @Test
    @Transactional
    public void requestAccountTest() throws CRUDException {
        Group group = groupDao.get(2);
        assertEquals(2, accountDao.requestAccount(group).size());
    }

    @Test
    @Transactional
    public void isModeratorTest() throws CRUDException {
        Account account1 = accountDao.get(4);
        Group group = groupDao.get(4);
        assertTrue(accountDao.isModerator(account1, group));
    }

    @Test
    @Transactional
    public void loginValidationTest() {
        accountDao.insert(ac);
        assertEquals(ac, accountDao.loginValidation("brightDays@mail.ru", "12431cdd23d23d23ed23d23"));
    }

    @Test
    @Transactional
    public void acceptInGroupTest() throws CRUDException {
        Account applicant = accountDao.get(5);
        Group group = groupDao.get(4);
        int before = groupDao.getAccounts(group).size();
        accountDao.joinGroup(applicant, group);
        accountDao.acceptInGroup(applicant, group);
        assertEquals(before + 1, groupDao.getAccounts(group).size());
    }

    @Test
    @Transactional
    public void loadPictureTest() throws IOException {
        Account account = accountDao.get(1);
        accountDao.loadPicture(account, null);
    }

    @Test
    @Transactional
    public void getFriendsTest() throws CRUDException {
        Account account = accountDao.get(2);
        assertEquals(2, accountDao.getFriends(account).size());
    }

    @Test
    @Transactional
    public void acceptFriendshipTest() throws CRUDException {
        Account account2 = accountDao.get(2);
        Account account5 = accountDao.get(5);
        accountDao.acceptFriendship(account2, account5);
        assertEquals(3, accountDao.getFriends(account2).size());
    }

    @Test
    @Transactional
    public void requestForFriendTest() throws CRUDException {
        Account account5 = accountDao.get(5);
        assertEquals(2, accountDao.requestForFriend(account5).size());
    }

    @Test
    @Transactional
    public void proposeFriendshipTest() throws CRUDException {
        Account account5 = accountDao.get(5);
        Account account3 = accountDao.get(3);
        accountDao.proposeFriendship(account3, account5, account5);
        assertEquals(3, accountDao.requestForFriend(account5).size());
    }

    @Test
    @Transactional
    public void getFriendStatusTest() throws CRUDException {
        Account account1 = accountDao.get(2);
        Account account2 = accountDao.get(5);
        assertEquals("PENDING", accountDao.getFriendStatus(account1, account2));
    }
}
