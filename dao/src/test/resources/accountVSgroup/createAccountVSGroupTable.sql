CREATE TABLE accountVSgroup (
account_group_id INT(10) NOT NULL AUTO_INCREMENT,
idAccount bigint(20) DEFAULT NULL,
idGroup bigint(20) DEFAULT NULL ,
status VARCHAR(15) DEFAULT 'VIEWER',
PRIMARY KEY (account_group_id),
CONSTRAINT unique_field UNIQUE(idAccount, idGroup),
CONSTRAINT idAccFK FOREIGN KEY(idAccount) REFERENCES account(id) ON DELETE SET NULL ,
CONSTRAINT adGpFK FOREIGN KEY(idGroup) REFERENCES groups(id) ON DELETE SET NULL
);