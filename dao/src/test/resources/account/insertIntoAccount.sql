INSERT INTO `account`
(`name`,`surname`,`middle_name`,`birth_date`,`home_address`,
`work_address`,`email`,`icq`,`skype`,`extra_info`)
VALUES
('Alex','Petrov','Sergeevich', '1990-09-09',
'Baker street', 'Kremil 1','alextPetrov@mail.ru',
'123123123','callMeMayBe', 'Love to live'),
('Semen','Korolev','Ivanovich', '1991-04-10',
'Vyazov street','White house','GreatSemen@mail.ru',
'321321321','7days', 'official'),
('Max','Pain','Arkadievich', '1992-01-07',
'Bruklin street', 'dark place','maxPain@mail.ru',
'987654321','maximus', 'have some enemies'),
('Arteem','Savelev','Michailovich', '1995-01-11',
'Manheton street', 'Google inc','Artem@mail.ru',
'9876211221','artemius', 'like football'),
('Ibragim','Kiselev','Sergeevich', '1988-04-07',
'Mayamee street', 'Uta','Ibragim@mail.ru',
'9871234321','kisel', 'waatch TV');

