CREATE TABLE `account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `middle_name` varchar(20) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `home_address` varchar(25) DEFAULT NULL,
  `work_address` varchar(25) DEFAULT NULL,
  `email` varchar(20) NOT NULL,
  `icq` char(10) DEFAULT NULL,
  `skype` varchar(20) DEFAULT NULL,
  `extra_info` varchar(25) DEFAULT NULL,
  `password` char(64) DEFAULT NULL,
  picture mediumblob DEFAULT null,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
