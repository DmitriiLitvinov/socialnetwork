CREATE TABLE groups (
id bigint(20) NOT NULL AUTO_INCREMENT,
name VARCHAR(30) NOT NULL,
founder bigint(20) DEFAULT NULL,
description varchar(40) NULL,
picture MEDIUMBLOB NULL ,
PRIMARY KEY(id),
UNIQUE KEY unique_key(name),
CONSTRAINT id_fk FOREIGN KEY(founder)
REFERENCES account(id) ON DELETE SET NULL
);