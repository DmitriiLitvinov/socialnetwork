CREATE TABLE phone (
num char(10) not NULL PRIMARY KEY,
person bigint(20) DEFAULT null,
type varchar(10) not null,
CONSTRAINT person_fk FOREIGN KEY(person)
REFERENCES account(id) ON DELETE SET NULL
);
