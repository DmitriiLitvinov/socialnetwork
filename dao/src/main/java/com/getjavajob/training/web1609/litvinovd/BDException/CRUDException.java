package com.getjavajob.training.web1609.litvinovd.BDException;

/**
 * Created by ds.litvinov on 19.11.2016.
 */
public class CRUDException extends Exception {
    public CRUDException(String str) {
        super(str);
    }
}
