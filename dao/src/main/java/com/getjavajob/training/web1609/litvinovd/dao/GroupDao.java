package com.getjavajob.training.web1609.litvinovd.dao;

import com.getjavajob.training.web1609.litvinovd.entity.Account;
import com.getjavajob.training.web1609.litvinovd.entity.AccountAndGroup;
import com.getjavajob.training.web1609.litvinovd.entity.AccountGroupStatus;
import com.getjavajob.training.web1609.litvinovd.entity.Group;
import org.springframework.stereotype.Repository;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ds.litvinov on 19.11.2016.
 */
@Repository
public class GroupDao extends AbstractDao<Group> {

    public GroupDao() {
    }

    @Override
    public Class<Group> getEntityClass() {
        return Group.class;
    }


    public List<Account> getAccounts(Group group) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AccountAndGroup> cq = cb.createQuery(AccountAndGroup.class);
        Root<AccountAndGroup> root = cq.from(AccountAndGroup.class);
        cq.where(cb.and(cb.equal(root.get("group"), group), cb.equal(root.get("status"), AccountGroupStatus.MEMBER)));
        TypedQuery<AccountAndGroup> query = em.createQuery(cq);
        List<Account> result = new ArrayList<>();
        for (AccountAndGroup accountAndGroup : query.getResultList()) {
            result.add(accountAndGroup.getAccount());
        }
        return result;
    }

    public byte[] getBytePicture(Group group) {
        return em.find(getEntityClass(), group.getId()).getBytePicture();
    }
}

