package com.getjavajob.training.web1609.litvinovd.dao;

import com.getjavajob.training.web1609.litvinovd.entity.IdManipulation;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ds.litvinov on 17.11.2016.
 */
@Repository
public abstract class AbstractDao<T extends IdManipulation> {

    @PersistenceContext
    protected EntityManager em;

    protected AbstractDao() {
    }

    protected abstract Class<T> getEntityClass();

    public T get(long key) {
        return em.find(getEntityClass(), key);
    }

    public List<T> getAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(getEntityClass());
        Root<T> root = cq.from(getEntityClass());
        cq.select(root);
        return em.createQuery(cq).getResultList();
    }

    public T insert(T obj) {
        em.persist(obj);
        return obj;
    }

    public void delete(T obj) {
        T removedObject = em.find(getEntityClass(), obj.getId());
        em.remove(removedObject);
    }

    public void update(T obj) {
        em.merge(obj);
    }

    public List<T> searchByName(Map<String, String> attributes, int offset, int limit) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(getEntityClass());
        Root<T> root = cq.from(getEntityClass());
        List<Predicate> predicates = new ArrayList<>();
        attributes.keySet().forEach(s -> {
            if (root.get(s) != null) {
                predicates.add(cb.like(root.get(s), "%" + attributes.get(s) + "%"));
            }
        });
        cq.where(cb.or(predicates.toArray(new Predicate[]{})));
        TypedQuery<T> tq = em.createQuery(cq).setFirstResult(offset - 1).setMaxResults(limit);
        return tq.getResultList();
    }

    public long getNumberOfRow(Map<String, String> attributes) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<T> root = cq.from(getEntityClass());
        List<Predicate> predicates = new ArrayList<>();
        attributes.keySet().forEach(s -> {
            if (root.get(s) != null) {
                predicates.add(cb.like(root.get(s), "%" + attributes.get(s) + "%"));
            }
        });
        cq.select(cb.count(root));
        cq.where(cb.or(predicates.toArray(new Predicate[]{})));
        return em.createQuery(cq).getSingleResult();
    }
}

