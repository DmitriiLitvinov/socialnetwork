package com.getjavajob.training.web1609.litvinovd.dao;

import com.getjavajob.training.web1609.litvinovd.entity.*;
import org.springframework.stereotype.Repository;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.io.IOException;
import java.util.*;

/**
 * Created by ds.litvinov on 17.11.2016.
 */
@Repository
public class AccountDao extends AbstractDao<Account> {

    public AccountDao() {
    }

    @Override
    protected Class<Account> getEntityClass() {
        return Account.class;
    }

    public void sendWallMessage(Account sender, Account receiver, String text) {
        Account accountSender = em.find(Account.class, sender.getId());
        Account accountReceiver = em.find(Account.class, receiver.getId());
        WallMessage message = new WallMessage();
        message.setDate(new Date());
        message.setSender(accountSender);
        message.setReceiver(accountReceiver);
        message.setText(text);
        em.persist(message);
    }

    public Set<WallMessage> readWallMessage(Account account1) {
        Account ac1 = em.find(Account.class, account1.getId());
        Set<WallMessage> result = new TreeSet<>();
        for (WallMessage message : ac1.getWallSenderMessages()) {
            if (message.getReceiver().equals(ac1)) {
                result.add(message);
            }
        }
        for (WallMessage message : ac1.getWallReceiverMessages()) {
            if (message.getReceiver().equals(ac1)) {
                result.add(message);
            }
        }
        return result;
    }

    public void deleteWallMessage(long id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaDelete<WallMessage> cd = cb.createCriteriaDelete(WallMessage.class);
        Root<WallMessage> root = cd.from(WallMessage.class);
        cd.where(cb.equal(root.get("id"), id));
        em.createQuery(cd).executeUpdate();
    }

    public void sendMessage(Account sender, Account receiver, String text) {
        Account account1 = em.find(Account.class, sender.getId());
        Account account2 = em.find(Account.class, receiver.getId());
        Message message = new Message();
        message.setDate(new Date());
        message.setFrom(account1);
        message.setReceiver(account2);
        message.setText(text);
        em.persist(message);
    }

    public Set<Message> readMessage(Account account1, Account account2) {
        Account ac1 = em.find(Account.class, account1.getId());
        Account ac2 = em.find(Account.class, account2.getId());
        Set<Message> result = new TreeSet<>();
        for (Message message : ac1.getFromMessages()) {
            if (message.getReceiver().equals(ac2)) {
                result.add(message);
            }
        }
        for (Message message : ac1.getReceiverMessages()) {
            if (message.getFrom().equals(ac2)) {
                result.add(message);
            }
        }
        return result;
    }

    public List<Account> getFriends(Account account) {
        Account persistentAccount = em.find(Account.class, account.getId());
        List<Account> result = new ArrayList<>();
        for (Friend friends : persistentAccount.getWhoFriends()) {
            if (friends.getFriendStatus() == FriendStatus.FRIEND) {
                result.add(friends.getWithWhom());
            }
        }
        for (Friend friends : persistentAccount.getWithWhom()) {
            if (friends.getFriendStatus() == FriendStatus.FRIEND) {
                result.add(friends.getWho());
            }
        }
        return result;
    }

    public void proposeFriendship(Account account1, Account account2, Account confirm) {
        Account who = em.find(Account.class, account1.getId());
        Account withWhom = em.find(Account.class, account2.getId());
        Account confirmAccount = em.find(Account.class, confirm.getId());
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Friend> cq = cb.createQuery(Friend.class);
        Root<Friend> root = cq.from(Friend.class);
        cq.where(cb.and(cb.equal(root.get("who"), account1), cb.equal(root.get("withWhom"), account2),
                cb.equal(root.get("friendStatus"), FriendStatus.STRANGER)));
        if (em.createQuery(cq).getResultList().size() > 0) {
            Friend friends = em.createQuery(cq).getSingleResult();
            friends.setFriendStatus(FriendStatus.PENDING);
            friends.setConfirm(confirmAccount);
            em.merge(friends);
        } else {
            Friend requestFriend = new Friend();
            requestFriend.setWho(who);
            requestFriend.setWithWhom(withWhom);
            requestFriend.setConfirm(confirmAccount);
            requestFriend.setFriendStatus(FriendStatus.PENDING);
            em.persist(requestFriend);
        }
    }

    public void acceptFriendship(Account account1, Account account2) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Friend> cq = cb.createQuery(Friend.class);
        Root<Friend> root = cq.from(Friend.class);
        cq.where(cb.and(cb.equal(root.get("who"), account1), cb.equal(root.get("withWhom"), account2),
                cb.equal(root.get("friendStatus"), FriendStatus.PENDING)));
        Friend friends = em.createQuery(cq).getSingleResult();
        friends.setFriendStatus(FriendStatus.FRIEND);
        em.merge(friends);
    }

    public List<Account> requestForFriend(Account account) {
        Account persistentAccount = em.find(Account.class, account.getId());
        List<Account> result = new ArrayList<>();
        for (Friend friends : persistentAccount.getConfirm()) {
            if (friends.getFriendStatus() == FriendStatus.PENDING) {
                if (friends.getConfirm().equals(friends.getWho())) {
                    result.add(friends.getWithWhom());
                } else
                    result.add(friends.getWho());
            }
        }
        return result;
    }

    public String getFriendStatus(Account account1, Account account2) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Friend> cq = cb.createQuery(Friend.class);
        Root<Friend> root = cq.from(Friend.class);
        cq.where(cb.and(cb.equal(root.get("who"), account1), cb.equal(root.get("withWhom"), account2)));
        List<Friend> friends = em.createQuery(cq).getResultList();
        if (friends.size() == 0) {
            return "STRANGER";
        } else {
            return friends.get(0).getFriendStatus().toString();
        }
    }

    public List<Group> getGroups(Account account) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Group> cq = cb.createQuery(Group.class);
        Root<Group> root = cq.from(Group.class);
        Join<AccountAndGroup, Group> join = root.join("accountAndGroups");
        cq.where(cb.and(cb.equal(join.get("account"), account), cb.equal(join.get("status"), AccountGroupStatus.MEMBER)));
        return em.createQuery(cq).getResultList();
    }

    @Override
    public void update(Account account) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaDelete<Phone> cd = cb.createCriteriaDelete(Phone.class);
        Root<Phone> root = cd.from(Phone.class);
        cd.where(cb.equal(root.get("owner"), account));
        em.createQuery(cd).executeUpdate();
        for (Phone phone : account.getPhones()) {
            phone.setOwner(account);
        }
        em.merge(account);
    }

    public void joinGroup(Account account, Group group) {
        Account persistentAccount = em.find(Account.class, account.getId());
        Group persistentGroup = em.find(Group.class, group.getId());
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AccountAndGroup> cq = cb.createQuery(AccountAndGroup.class);
        Root<AccountAndGroup> root = cq.from(AccountAndGroup.class);
        cq.where(cb.and(cb.equal(root.get("account"), account), cb.equal(root.get("group"), group),
                cb.equal(root.get("status"), AccountGroupStatus.VIEWER)));
        TypedQuery<AccountAndGroup> tq = em.createQuery(cq);
        if (tq.getResultList().size() > 0) {
            AccountAndGroup accountAndGroup = tq.getSingleResult();
            accountAndGroup.setStatus(AccountGroupStatus.REQUEST);
            em.merge(accountAndGroup);
        } else {
            AccountAndGroup accountAndGroup = new AccountAndGroup();
            accountAndGroup.setAccount(persistentAccount);
            accountAndGroup.setGroup(persistentGroup);
            accountAndGroup.setStatus(AccountGroupStatus.REQUEST);
            em.persist(accountAndGroup);
        }
    }

    public List<Account> requestAccount(Group group) {
        List<Account> result = new ArrayList<>();
        for (AccountAndGroup accountAndGroup : em.find(Group.class, group.getId()).getAccountAndGroups()) {
            if (accountAndGroup.getStatus() == AccountGroupStatus.REQUEST) {
                result.add(accountAndGroup.getAccount());
            }
        }
        return result;
    }

    public boolean isModerator(Account moderator, Group group) {
        return em.find(Group.class, group.getId()).getFounder().equals(moderator);
    }

    public String getGroupStatus(Account account, Group group) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AccountAndGroup> cq = cb.createQuery(AccountAndGroup.class);
        Root<AccountAndGroup> root = cq.from(AccountAndGroup.class);
        cq.where(cb.and(cb.equal(root.get("account"), account), cb.equal(root.get("group"), group)));
        List<AccountAndGroup> accountAndGroup = em.createQuery(cq).getResultList();
        if (accountAndGroup.size() == 0) {
            return "VIEWER";
        } else {
            return accountAndGroup.get(0).getStatus().toString();
        }
    }

    public void rejectGroup(Account account, Group group) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AccountAndGroup> cq = cb.createQuery(AccountAndGroup.class);
        Root<AccountAndGroup> root = cq.from(AccountAndGroup.class);
        cq.where(cb.and(cb.equal(root.get("account"), account), cb.equal(root.get("group"), group),
                cb.equal(root.get("status"), AccountGroupStatus.REQUEST)));
        AccountAndGroup accountAndGroup = em.createQuery(cq).getSingleResult();
        accountAndGroup.setStatus(AccountGroupStatus.VIEWER);
        em.merge(accountAndGroup);
    }

    public void acceptInGroup(Account account, Group group) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AccountAndGroup> cq = cb.createQuery(AccountAndGroup.class);
        Root<AccountAndGroup> root = cq.from(AccountAndGroup.class);
        cq.where(cb.and(cb.equal(root.get("account"), account), cb.equal(root.get("group"), group),
                cb.equal(root.get("status"), AccountGroupStatus.REQUEST)));
        AccountAndGroup accountAndGroup = em.createQuery(cq).getSingleResult();
        System.out.println(accountAndGroup);
        accountAndGroup.setStatus(AccountGroupStatus.MEMBER);
        em.merge(accountAndGroup);
    }

    public Account loginValidation(String email, String password) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Account> cq = cb.createQuery(Account.class);
        Root<Account> root = cq.from(Account.class);
        cq.where(cb.and(cb.equal(root.get("email"), email), cb.equal(root.get("password"), password)));
        TypedQuery<Account> tQuery = em.createQuery(cq);
        if (tQuery.getResultList().size() == 0) {
            return null;
        }
        return tQuery.getSingleResult();
    }

    public void loadPicture(Account account, byte[] bytes) throws IOException {
        Account persistentAccount = em.find(getEntityClass(), account.getId());
        persistentAccount.setBytePicture(bytes);
        em.merge(persistentAccount);
    }

    public byte[] getBytePicture(Account account) {
        Account persistentAccount = em.find(getEntityClass(), account.getId());
        return persistentAccount.getBytePicture();
    }


    public void deleteFriend(Account ac1, Account ac2) {
        Account account1 = em.find(Account.class, ac1.getId());
        Account account2 = em.find(Account.class, ac2.getId());
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Friend> cq = cb.createQuery(Friend.class);
        Root<Friend> root = cq.from(Friend.class);
        cq.where(cb.and(cb.equal(root.get("who"), account1), cb.equal(root.get("withWhom"), account2),
                cb.equal(root.get("friendStatus"), FriendStatus.FRIEND)));
        Friend friends = em.createQuery(cq).getSingleResult();
        friends.setFriendStatus(FriendStatus.STRANGER);
        em.merge(friends);
    }
}

