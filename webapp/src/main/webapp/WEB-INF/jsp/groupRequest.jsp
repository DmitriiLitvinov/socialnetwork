<%@ page import="com.getjavajob.training.web1609.litvinovd.entity.Group" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Request account</title>
</head>
<body>
<%@ include file="header.jsp" %>
<div class="container">
    <div class="col-md-offset-2 col-md-6">
        <h3>Request account</h3>
        <c:forEach var="entity" items="${accountAndPicture}">
            <c:set var="account" value="${entity.key}"/>
            <c:set var="picture" value="${entity.value}"/>
            <div class="well row">
                <div class="col-md-9">
                    <a href="${pageContext.request.contextPath}/login/accountPage?accountId=${account.id}">
                        <img style="width:60px" src="data:image/jpeg;base64,${picture}"/>
                    </a>
                    <h4><small>Name:</small>${account.name}</h4>
                    <h4><small>Surname:</small>${account.surname}</h4>
                </div>
                <a href="${pageContext.request.contextPath}/login/acceptAccountGroup?groupId=${groupId}&accountId=${account.id}" class="btn btn-success" role="button">Accept!</a>
                <a href="${pageContext.request.contextPath}/login/rejectAccountGroup?groupId=${groupId}&accountId=${account.id}" class="btn btn-success" role="button">Reject</a>
            </div>
        </c:forEach>
        <a href="${pageContext.request.contextPath}/login/group?groupId=${groupId}" class="btn btn-success" role="button">Back to group</a>

    </div>
</div>
</body>
</html>