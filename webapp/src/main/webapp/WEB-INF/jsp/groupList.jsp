<%@ page import="com.getjavajob.training.web1609.litvinovd.entity.Group" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>My groups</title>
    <style>
        .btn-default {
            border-color: #cc7a00;
            background-color: #cc7a00;
        }
    </style>
</head>
<body>
<%@ include file="header.jsp" %>
<div class="container">
    <div class="col-md-offset-2 col-md-6">
        <h3>Groups</h3>
        <c:forEach var="entity" items="${groups}">
            <c:set var="group" value="${entity.key}"/>
            <c:set var="isModerator" value="${entity.value}"/>
            <div class="well row">
                <div class="col-md-6">
                    <a href="${pageContext.request.contextPath}/login/group?groupId=${group.id}">
                        <img style="width:60px" src="data:image/jpeg;base64,${group.stringPicture}"/>
                    </a>
                    <h4>
                        <small>Name:</small>
                            ${group.name}</h4>
                </div>
            </div>
        </c:forEach>
    </div>
</div>
<div class="container">
    <div class="col-md-offset-6" style="padding-bottom: 30px" >
        <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#myModal">Create group</button>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><small>Create group</small></h4>
            </div>
            <div class="modal-body">
                <form action="${pageContext.request.contextPath}/login/addGroup" method="post">
                    <div class="form-group">
                        <label class="control-label">Group name:</label>
                        <input type="text" class="form-control" name="name">
                        <label for="comment">Description:</label>
                        <textarea class="form-control" rows="5" id="comment" name="description"></textarea>
                    </div>
                    <button type="submit" class="btn btn-default" >Create group</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
