<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/sources/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/sources/jquery/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/sources/jquery/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/sources/jquery/jquery-ui.css"/>

</head>
<body>
<c:set var="count" value="0" scope="page"/>

<form:form action="${pageContext.request.contextPath}/updatePhones" method="post" modelAttribute="account">
    <div id="phoneDiv">
        <c:forEach var="phone" items="${account.phones}" varStatus="status">
            <c:set var="count" value="${count + 1}" scope="page"/>
            <div id="d${count}">
                <form:select path="phones[${status.index}].type" size="1" multiple="">
                    <c:forEach var="type" items="${phoneTypes}">
                        <c:choose>
                            <c:when test="${phone.type == type}">
                                <form:option value="${type}" selected="true">${type}</form:option>
                            </c:when>
                            <c:otherwise>
                                <form:option value="${type}">${type}</form:option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </form:select>
                <form:input path="phones[${status.index}].num" value="${phone.num}"/>
                <button class="deleteButton" type="button" alt="${count}">remove</button>
            </div>
            <br>
        </c:forEach>
    </div>
    <input type="submit" value="Submit">
</form:form>
<button type="button" id="addPhone">addPhone</button>
<script>
    var counter = ${count + 1};
    $(document).ready(function () {
        $("#phoneDiv").on("click", "button.deleteButton", function () {
            var v = $(this).attr("alt");
            $("#d" + v + "").remove();

        });
        $("#addPhone").click(function () {
            $("#phoneDiv").append("<div id=\"d" + counter + "\">" +
                    "<label>Type: </label>" +
                    "<select name=\"phones[" + counter + "].type\">" +
                    "<option value=\"MOBILE\">Mobile</option>" +
                    "<option value=\"HOME\">Home</option>" +
                    "<option value=\"WORK\">Work</option>" +
                    "</select>" +
                    "<input type=\"text\" name=\"phones[" + counter + "].num\"" +
                    " placeholder=\"89001234567\">" +
                    "<button class=\"deleteButton\" type=\"button\" alt=\"" + counter + "\">remove</button></div>");
            counter++;
        });
    });
</script>

</body>
</html>
