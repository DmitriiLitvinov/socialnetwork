<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%@ include file="header.jsp" %>
<div class="container">
    <div class="col-md-offset-2 col-md-6">
        <h3>Search Result</h3>
        <ul class="nav nav-tabs">
        <li class="active"><a
                href="${pageContext.request.contextPath}/login/search/account/1?search=${search}">Accounts</a>
        </li>
        <li><a href="${pageContext.request.contextPath}/login/search/group/1?search=${search}">Groups</a>
        </li>
    </ul>
    </div>
    <div class="col-md-offset-2 col-md-6" id="empty">
        <c:forEach var="entity" items="${accountAndPicture}">
            <c:set var="account" value="${entity.key}"/>
            <c:set var="picture" value="${entity.value}"/>
            <div class="well row">
                <a href="${pageContext.request.contextPath}/login/accountPage?accountId=${account.id}">
                    <img style="width:60px" src="data:image/jpeg;base64,${picture}"/>
                </a>
                <h4><small>Name:</small>${account.name} ${account.surname}</h4>
            </div>
        </c:forEach>
    </div>
</div>
<div class="container">
    <div class="col-md-offset-2 col-md-6">
    <c:set var="num" value="${rows}" scope="page"/>
    <c:if test="${num > 1}">
        <ul class="pagination">
            <c:forEach var="i" begin="1" end="${rows}">
                <li><a class="page" alt="${i}">${i}</a></li>
            </c:forEach>
        </ul>
    </c:if>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".page").click(function () {
            var number = $(this).attr("alt");
            $("#empty").empty();
            $.ajax({
                url: "/login/search/test/" + number,
                data: "search=${search}",
                success: function (data) {
                    jQuery.each(data, function (index, item) {
                        $("#empty").append("<div class=\"well row\">" +
                                "<a href=\"${pageContext.request.contextPath}/login/accountPage?accountId=" + item.id + "\">" +
                                "<img style=\"width:60px\" src=\"data:image/jpeg;base64," + item.stringPicture + "\">" +
                                "</a><h4><small>Name:</small>" + item.label + "</h4></div>");
                    });
                }
            });

        });

    });
</script>

</body>
</html>
