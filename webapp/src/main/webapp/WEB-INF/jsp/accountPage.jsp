<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <title>Account Page</title>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/sources/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/sources/js/sockjs.js"></script>
    <script src="${pageContext.request.contextPath}/sources/js/stomp.js"></script>

</head>
<style>
    #wall {
        border: 1px solid grey;
        border-radius: 8px;
    }
</style>
<body>
<%@ include file="header.jsp" %>
<c:set var="myPage" value="${sessionScope.accountSession.id == account.id}" scope="page"/>
<c:set var="wallMsgNum" value="0" scope="page"/>
<div class="container">
    <div class="row">
    <div class="col-md-offset-2 col-md-3">
        <h4>${account.name} ${account.surname}</h4>
        <img style="width:100%" src="data:image/png;base64,${picture}" alt="Picture">
        <div class="list-group">
        <c:if test="${myPage}">
            <a class="list-group-item list-group-item-info"  href="${pageContext.request.contextPath}/login/loadToXml">Load to xml file</a>
        </c:if>
        <c:if test="${!myPage}">
            <a class="list-group-item list-group-item-info" onclick="connect();" data-toggle="modal" data-target="#myModal">Start chat</a>
        </c:if>
        <c:choose>
            <c:when test="${friendStatus == 'STRANGER' && !myPage}">
                <a class="list-group-item list-group-item-info" href="${pageContext.request.contextPath}/login/addFriend?id=${account.id}">Add to my friends</a>
            </c:when>
            <c:when test="${friendStatus == 'PENDING'}">
                <a class="list-group-item disabled list-group-item-info">Request was sent</a>
            </c:when>
            <c:when test="${friendStatus == 'FRIEND'}">
                <a class="list-group-item list-group-item-danger" href="${pageContext.request.contextPath}/login/deleteFriend?id=${account.id}">Remove from friends</a>
            </c:when>
        </c:choose>
                </li>
        </div>
    </div>
    <div class="col-md-3">
        <h4><small style="padding-right:43px">Name: </small>${account.name}</h4>
        <h4><small style="padding-right:28px">Surname:</small>${account.surname}</h4>
        <h4><small>Middle name: </small>${account.middleName}</h4>
        <h4><small style="padding-right:45px">Skype:</small>${account.skype}</h4>
        <h4><small style="padding-right:66px">icq:</small>${account.icq}</h4>
        <br>
    <c:if test="${fn:length(account.phones) gt 0}">
    <table class="table table-striped">
            <tr>
                <th>Type</th>
                <th>Number</th>
            </tr>
            <c:forEach var="phone" items="${account.phones}">
                <tr>
                    <td>${phone.type}</td>
                    <td>${phone.num}</td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
    </div>
    </div>
</div>
<div class="container">
    <div class="col-md-offset-2 col-md-6" id="wall">
        <%--action="${pageContext.request.contextPath}/login/wallMessage/" method="POST"--%>
        <form>
        <div class="form-group">
            <label for="wallComment">Comment:</label>
            <textarea class="form-control" rows="1" onclick="this.rows=5" id="wallComment"></textarea>
        </div>
            <button type="button" class="btn btn-info" value="send" id="wallSend">Send</button>
        </form>
            <div id="wallContent">
            </div>
            <c:forEach var="wallMessage" items="${wallMessages}">
                <div class="well row" id="wallMessage${wallMessage.id}">
                    <div class="col-md-3">
                    <a href="${pageContext.request.contextPath}/login/accountPage?accountId=${wallMessage.senderId}">
                    <img style="width:60px" src="data:image/jpeg;base64,${wallMessage.stringPicture}">
                    </a><h4><small>${wallMessage.from}</small></h4>
                    <h4><small>${wallMessage.date}</small></h4>
                    </div>
                    <div class="col-md-8" style="word-wrap: break-word"><h4>${wallMessage.text}</h4></div>
                    <c:if test="${myPage}">
                    <div class="col-md-1">
                        <button type="button" class="btn btn-danger deleteWallMsg" alt="${wallMessage.id}" >-</button>
                    </div>
                    </c:if>
                </div>
            </c:forEach>
    </div>
</div>
<%-- Chat dialog --%>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="disconnect()">&times;</button>
                <h4 class="modal-title"><small>Chat with</small> ${account.surname}</h4>
            </div>
            <div class="modal-body">
                <div id="fill" class="pre-scrollable">
                    <c:forEach var="msg" items="${messages}">
                        <c:set var="surname" value="${msg.from.surname}" scope="page"/>
                        <c:if test="${surname == sessionScope.accountSession.surname}">
                            <c:set var="surname" value="You" scope="page"/>
                        </c:if>
                        <div class="well" style="word-wrap: break-word">${surname} : ${msg.text}</div>
                    </c:forEach>
                </div>
                <form>
                    <div class="form-group">
                        <label for="comment">Comment:</label>
                        <textarea class="form-control" rows="5" id="comment"></textarea>
                    </div>
                </form>
                <button type="button" class="btn btn-default" onclick="sendMessage();" id="send">Send</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="disconnect()">Close</button>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="nickname" value="${sessionScope.accountSession.surname}"/>
</body>
<script>
    var stompClient = null;

    function connect() {

        var socket = new SockJS('/chat');
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {

            console.log('Connected: ' + frame);
            stompClient.subscribe('/topic/messages/${account.id}/${sessionScope.accountSession.id}', function (messageOutput) {

                showMessageOutput(JSON.parse(messageOutput.body));
            });
        });
    }

    function disconnect() {

        if (stompClient != null) {
            stompClient.disconnect();
        }

        setConnected(false);
        console.log("Disconnected");
    }

    function sendMessage() {

        var from = document.getElementById('nickname').value;
        var text = document.getElementById('comment').value;
        var time = time_format(new Date());

        $("#comment").val('');
        stompClient.send("/chat", {}, JSON.stringify({'from': from, 'text': text,
            'senderId':${sessionScope.accountSession.id}, 'receiverId' : ${account.id}}));
        $("#fill").append("<div class=\"well\" style=\"word-wrap: break-word;\">You : " + text + " (" + time + ")" + "</div>");
    }

    function showMessageOutput(messageOutput) {
        $("#fill").append("<div class=\"well\" style=\"word-wrap: break-word;\">" + messageOutput.from + ": " + messageOutput.text + " (" + messageOutput.time + ")" + "</div>");
    }

    function time_format(d) {
        hours = format_two_digits(d.getHours());
        minutes = format_two_digits(d.getMinutes());
        return hours + ":" + minutes;
    }

    function format_two_digits(n) {
        return n < 10 ? '0' + n : n;
    }
    function getDate() {
        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();

        var output = d.getFullYear() + '/' +
                (month<10 ? '0' : '') + month + '/' +
                (day<10 ? '0' : '') + day;
        return output;
    }
    $(document).ready(function () {

        $("#wall").on("click", "button.deleteWallMsg", function () {
          var num = $(this).attr("alt");
            $.ajax({
                url:"/login/wallMessage/delete?id=" + num,
                success : function(data) {
                    if (data) {
                        $("#wallMessage" + num).remove()
                    }
                }
            });
        });


        $("#wallSend").click(function () {
            var msg = $("#wallComment").val();
            var from = $("#nickname").val();
            $("#wallComment").val('');
            if(msg) {
            $.ajax({
                type: "POST",
                url: "/login/wallMessage",
                contentType : "application/json",
                dataType: "json",
                data: JSON.stringify({from: from, text: msg, senderId:${sessionScope.accountSession.id}, receiverId : ${account.id}, date : getDate()}),
                success: function (data) {
                    $("#wallContent").append("<div class=\"well row\" id=\"wallMessage\">" +
                                     "<div class=\"col-md-3\">" +
                                "<a href=\"${pageContext.request.contextPath}/login/accountPage?accountId=" + data.id + "\">" +
                                "<img style=\"width:60px\" src=\"data:image/jpeg;base64," + data.stringPicture + "\">" +
                                "</a><h4><small>" + data.type + "</small></h4>" +
                                "<h4><small>" + getDate() + "</small></h4>" +
                                "</div>" +
                            "<div class=\"col-md-9\" style=\"word-wrap: break-word\"><h4>" + data.label + "</h4></div>" +
                            "</div>");

                }
            });
            }
        });
    });
</script>
</html>
