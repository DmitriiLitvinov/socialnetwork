<%@ page import="com.getjavajob.training.web1609.litvinovd.entity.Group" %>
<%@ page import="com.getjavajob.training.web1609.litvinovd.logic.GroupService" %>
<%@ page session="false" %><%--
  Created by IntelliJ IDEA.
  User: ds.litvinov
  Date: 15.12.2016
  Time: 20:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <%@ include file="header.jsp"%>
    <h3>Edit Group</h3>
    <form action="${pageContext.request.contextPath}/login/updateGroup?groupId=${group.id}" method="post" enctype="multipart/form-data">
        Name:<input type="text" name="name" value="${group.name}"><br/>
        Description:<input type="text" name="description" value="${group.description}"><br/>
        <input type="file" name="file"/>
        <input type="submit" value="update">
    </form>
</body>
</html>
