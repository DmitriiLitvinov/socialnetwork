<%@ page import="com.getjavajob.training.web1609.litvinovd.entity.Group" %>
<%@ page import="com.getjavajob.training.web1609.litvinovd.logic.GroupService" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%@ include file="header.jsp" %>
<div class="container text-center">
    <div class="col-md-10">
        <h2>${group.name}</h2>
    </div>
</div>
<div class="container text-center">
    <div class="row">
        <div class="col-md-offset-2 col-md-3">
            <img style="width:100%" src="data:image/jpeg;base64,${picture}" alt="Picture">
            <div class="list-group">
                <c:if test="${isModerator != true}">
                    <c:choose >
                        <c:when test="${status == 'VIEWER'}">
                            <a class="list-group-item list-group-item-warning"
                               href="${pageContext.request.contextPath}/login/joinGroup?groupId=${group.id}">Join group</a>
                        </c:when>
                        <c:when test="${status == 'REQUEST'}">
                            <a class="list-group-item list-group-item-warning disabled">Request was sent</a>
                        </c:when>
                    </c:choose>

                </c:if>
                <a class="list-group-item list-group-item-warning"href="${pageContext.request.contextPath}/login/groupMember?groupId=${group.id}">Group members</a>
                <c:if test="${isModerator == true}">
                    <a class="list-group-item list-group-item-warning" data-toggle="modal" data-target="#myModal">Update group</a>
                    <c:if test="${numRequests > 0}">
                        <a class="list-group-item list-group-item-warning"
                           href="${pageContext.request.contextPath}/login/groupRequest?groupId=${group.id}">New requests<span
                                class="badge">${numRequests}</span></a>
                    </c:if>
                </c:if>
            </div>
        </div>
        <div class="col-md-3">
            <label>Description:</label>
            <div class="well">${group.description}</div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    <small>Update group</small>
                </h4>
            </div>
            <div class="modal-body">
                <form action="${pageContext.request.contextPath}/login/updateGroup?groupId=${group.id}" method="post"
                      enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label">Group name:</label>
                        <input type="text" class="form-control" name="name" value="${group.name}">
                        <input type="hidden" name="id" value="${group.id}">
                        <label for="updateComment">Description:</label>
                        <textarea class="form-control" rows="5" id="updateComment"
                                  name="description">${group.description}</textarea>
                        <input type="file" name="file"/>
                    </div>
                    <button type="submit" class="btn btn-default">Update group</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>
