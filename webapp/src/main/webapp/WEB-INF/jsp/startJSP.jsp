<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-flaud">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="registration.html"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                </ul>
            </div>
            <div class="col-sm-2"></div>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6" style="padding-bottom:30px">
            <c:if test="${not empty message}">
                <div style="color: red"><h4>${message}</h4></div>
            </c:if>
            <h1>Welcome!</h1></div>
        <div class="col-sm-3"></div>
    </div>
    <form form action="loginSer" method="post" class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-3" for="email">Email:</label>
            <div class="col-sm-6">
                <input type="email" class="form-control" id="email" name="userEmail" placeholder="Enter email">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="pwd">Password:</label>
            <div class="col-sm-6">
                <input type="password" class="form-control" id="pwd" name="userPass" placeholder="Enter password">
            </div>
        </div>
        <div class="col-sm-offset-3 col-sm-6" style="padding-bottom:20px">
            <div class="checkbox">
                <label><input type="checkbox" name="remember" value="yes" checked> Remember me</label>
            </div>
        </div>
        <div class="col-sm-offset-3 col-sm-6">
            <button type="submit" class="btn btn-default">Login</button>
        </div>
    </form>
</div>
</body>

</html>
