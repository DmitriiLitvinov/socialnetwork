<%@ page import="com.getjavajob.training.web1609.litvinovd.entity.Group" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Group members</title>
</head>
<body>
<%@ include file="header.jsp" %>
<div class="container">
    <div class="col-md-offset-2 col-md-6">
        <h3>Group members</h3>
        <c:forEach var="entity" items="${membersAndPictures}">
            <c:set var="account" value="${entity.key}"/>
            <c:set var="picture" value="${entity.value}"/>
            <div class="well row">
                <div class="col-md-6">
                    <a href="${pageContext.request.contextPath}/login/accountPage?accountId=${account.id}">
                        <img style="width:60px" src="data:image/jpeg;base64,${picture}"/>
                    </a>
                    <h4><small>Name:</small>${account.name}</h4>
                    <h4><small>Surname:</small>${account.surname}</h4>
                </div>
            </div>
        </c:forEach>
    </div>
</div>
</body>
</html>