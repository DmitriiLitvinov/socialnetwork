<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<html>
<head>
    <title>Edit</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<%@ include file="header.jsp" %>

<script src="${pageContext.request.contextPath}/js/edit.js"></script>

<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6" style="padding-bottom:30px"><h1>Edit account info</h1></div>
        <div class="col-md-4"></div>
    </div>
    <div class="well">
        <form:form action="${pageContext.request.contextPath}/login/accountPage/updatePage" method="post"
                   class="form-horizontal" id="target" modelAttribute="account">
            <div class="form-group">
                <label class="control-label col-md-3">Name:</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="name" value="${account.name}">
                    <input type="hidden" name="id" value="${accountSession.id}"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Surname:</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="surname" value="${account.surname}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Middle name:</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="middleName" value="${account.middleName}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Email:</label>
                <div class="col-md-6">
                    <input type="email" class="form-control" name="email" value="${account.email}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Skype:</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="skype" value="${account.skype}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">icq:</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="icq" value="${account.icq}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Password*:</label>
                <div class="col-md-6">
                    <input type="password" class="form-control" name="password" required id="password">
                </div>
            </div>
            <div class="form-group form-inline" id="phoneDiv">
                <c:set var="count" value="0" scope="page"/>
                <c:forEach var="phone" items="${account.phones}" varStatus="status">
                    <c:set var="count" value="${count + 1}" scope="page"/>
                    <div id="d${count}">
                        <label class="control-label col-md-3">Phone:</label>
                        <div class="col-md-9">
                            <label>Type: </label>
                            <form:select class="form-control" path="phones[${status.index}].type" size="1"
                                         multiple="">
                                <c:forEach var="type" items="${phoneTypes}">
                                    <c:set var="phoneT" value="${type}"/>
                                    <c:choose>
                                        <c:when test="${phone.type == type}">
                                            <form:option value="${type}"
                                                         selected="true">${fn:toLowerCase(phoneT)}</form:option>
                                        </c:when>
                                        <c:otherwise>
                                            <form:option value="${type}">${fn:toLowerCase(phoneT)}</form:option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </form:select>
                            <label class="control-label">Number: </label>
                            <form:input class="form-control blur" path="phones[${status.index}].num"
                                        value="${phone.num}"/>
                            <button class="btn btn-danger deleteButton" type="button" alt="${count}">-</button>
                        </div>
                    </div>
                </c:forEach>
                <input type="hidden" value="${count + 1}" id="hiddenCount"/>
            </div>
            <div class="col-md-offset-7" style="padding-left: 36px">
                <button id="addPhone" class="btn btn-success" type="button">+</button>
            </div>
            <br/>
            <br/>
            <div class="text-center">
                <button type="button" class="btn btn-info" value="update" id="opener">Update</button>
            </div>
        </form:form>
    </div>

    <div id="dialog" title="Минуточку!">
        Уверены ли вы, что хотите сохранить изменения?
    </div>
    <div id="wrongPhone" title="Ошибка!">
        Неправильный номер телефона!
    </div>
    <div id="EmptyPassword" title="Ошибка!">
        Поле password не может быть пустым!
    </div>
    <hr/>
</div>
<div class="container">
    <form class="form-incline" action="${pageContext.request.contextPath}/login/loadAccountFromXml" method="post"
          enctype="multipart/form-data">
        <div class="form-group">
            <label>Load account from Xml</label>
            <input type="file" name="file" class="form-control">
        </div>
        <div class="form-group" style="padding-bottom: 20px">
            <input type="submit" class="btn btn-info" value="Load from Xml"/>
        </div>
    </form>
</div>
<div class="container">
    <form class="form-incline" action="${pageContext.request.contextPath}/login/upload" method="post"
          enctype="multipart/form-data">
        <div class="form-group">
            <label>Update picture</label>
            <input type="file" name="file" class="form-control">
        </div>
        <div class="form-group" style="padding-bottom: 20px">
            <input type="submit" class="btn btn-info" value="Load picture"/>
        </div>
    </form>
</div>

</body>

</html>
