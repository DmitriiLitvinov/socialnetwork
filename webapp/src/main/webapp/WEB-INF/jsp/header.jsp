<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/sources/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/sources/jquery/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/sources/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/sources/jquery/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/sources/jquery/jquery-ui.css"/>

    <script src="${pageContext.request.contextPath}/js/searchLine.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Network</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a
                            href="${pageContext.request.contextPath}/login/accountPage?accountId=${sessionScope.accountSession.id}">HOME</a>
                    </li>
                    <li><a id="friend" href="${pageContext.request.contextPath}/login/getFriends">Friends<span class="badge"></span></a></li>
                    <li><a href="${pageContext.request.contextPath}/login/groups">Groups</a></li>
                    <li><a href="${pageContext.request.contextPath}/login/accountPage/edit">Edit</a></li>
                </ul>
                <form action="${pageContext.request.contextPath}/login/search/account/1" method="get"
                      class="navbar-form navbar-left">
                    <div class="form-group">
                        <input type="text" name="search" class="form-control" placeholder="Search" id="tags">
                    </div>
                    <button type="submit" class="btn btn-default">Find</button>
                </form>
                <ul class="nav navbar-nav navbar-right">
                    <li><p class="navbar-text">${sessionScope.accountSession.email}</p></li>
                    <li><a href="${pageContext.request.contextPath}/logout"><span
                            class="glyphicon glyphicon-log-out"></span> Log out</a></li>
                </ul>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</nav>
</body>
<script>
    $(document).ready(function(){
            $.ajax({url: "/login/requestNum", success: function(result){
                var v = result;
                if(v > 0) {
                    $(".badge").html(result);
                    $("#friend").attr("href", "${pageContext.request.contextPath}/login/getRequests");
                }
            }});
    });
</script>
</html>
