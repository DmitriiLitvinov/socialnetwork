$(document).ready(function () {
    $("#tags").autocomplete({
        source: function (request, response) {
            $.get("/login/search/autocomplete?name=" + request.term, function (data) {
                response(data);
            });
        },
        select: function (event, ui) {
            if (ui.item.type == 'account') {
                location.href = "/login/accountPage?accountId=" + ui.item.id;
            } else {
                location.href = "/login/group?groupId=" + ui.item.id;
            }
        },

    }).data("ui-autocomplete")._renderMenu = function (ul, items) {
        var that = this;
        var type = null;
        $.each(items, function (index, item) {
            if (item.type != type) {
                type = item.type;
                ul.append('<li><b>' + type + ':</b></li>');
            }
            that._renderItemData(ul, item);
        });

    };
});
