/**
 * Created by ds.litvinov on 23.01.2017.
 */
$(document).ready(function () {
    var counter = document.getElementById("hiddenCount").value;
    $("#addPhone").click(function () {
        $("#phoneDiv").append("<div id=\"d" + counter + "\"><label class=\"control-label col-md-3\">Phone: </label>" +
            "<div class=\"col-md-9\">" +
            "<label>Type:&nbsp</label>" +
            "<select class=\"form-control\" name=\"phoneList[" + counter + "].type\">" +
            "<option value=\"MOBILE\">mobile</option>" +
            "<option value=\"HOME\">home</option>" +
            "<option value=\"WORK\">work</option>" +
            "</select>" +
            "<label class=\"control-label\">&nbspNumber:&nbsp</label>" +
            "<input type=\"text\" class=\"form-control blur\"  name=\"phoneList[" + counter + "].num\"" +
            " placeholder=\"89001234567\">&nbsp" +
            "<button class=\"btn btn-danger deleteButton\" type=\"button\" alt=\"" + counter + "\">-</button></div></div>");
        counter++;
    });
    $(".form-group").on("click", "button.deleteButton", function () {
        var v = $(this).attr("alt");
        $("#d" + v).remove();
    });

    $("#phoneDiv").on("blur", "input.blur", function () {
        var x = $(this).val();
        var patt = /^89[0-9]{9}$/;
        if (!patt.test(x) && x) {
            $("#wrongPhone").dialog("open");
            $(this).val('');
        }
    });
    $("#dialog").dialog({
        autoOpen: false,
        buttons: {
            Да: function () {
                $(this).dialog("close");
                $("#target").submit();
            },
            Нет: function () {
                $(this).dialog("close");
            }
        }

    });
    $("#opener").click(function () {
        var value = $("#password").val();
        if (!value) {
            $("#EmptyPassword").dialog("open");
        } else {
            $("#dialog").dialog("open");
        }
    });

    $("#wrongPhone").dialog({
        autoOpen: false,
        buttons: {
            ОК: function () {
                $(this).dialog("close");
            }
        }
    });
    $("#EmptyPassword").dialog({
        autoOpen: false,
        buttons: {
            ОК: function () {
                $(this).dialog("close");
            }
        }
    });
});
