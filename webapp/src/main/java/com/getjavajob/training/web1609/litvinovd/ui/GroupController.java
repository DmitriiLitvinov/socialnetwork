package com.getjavajob.training.web1609.litvinovd.ui;

import com.getjavajob.training.web1609.litvinovd.entity.Account;
import com.getjavajob.training.web1609.litvinovd.entity.Group;
import com.getjavajob.training.web1609.litvinovd.logic.AccountService;
import com.getjavajob.training.web1609.litvinovd.logic.GroupService;
import com.getjavajob.training.web1609.litvinovd.logic.util.AccountMessage;
import com.getjavajob.training.web1609.litvinovd.logic.util.AccountServiceException;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ds.litvinov on 24.01.2017.
 */
@Controller
@SessionAttributes("accountSession")
public class GroupController {

    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);
    @Autowired
    private GroupService groupService;
    @Autowired
    private AccountService accountService;

    @PostMapping("/login/addGroup")
    public String addGroup(@ModelAttribute Group group, @ModelAttribute("accountSession") Account account) throws IOException {
        logger.info("create new group");
        group.setFounder(account);
        DataInputStream stream = new DataInputStream(this.getClass().getClassLoader().getResourceAsStream("group_default.jpg"));
        group.setBytePicture(IOUtils.toByteArray(stream));
        stream.close();
        try {
            group = groupService.createGroup(account, group);
        } catch (AccountServiceException e) {
            e.printStackTrace();
        }
        return "redirect:/login/group?groupId=" + group.getId();
    }

    @RequestMapping("/login/group")
    public ModelAndView groupPage(@ModelAttribute("accountSession") Account account,
                                  @RequestParam("groupId") long id) {
        logger.info("open group with id = " + id);
        Group group = groupService.get(id);
        ModelAndView modelAndView = new ModelAndView("group");
        modelAndView.addObject("group", group);
        modelAndView.addObject("picture", groupService.getStringPicture(group));
        modelAndView.addObject("isModerator", accountService.isModerator(account, group));
        int numRequests = accountService.requestAccount(group).size();
        modelAndView.addObject("numRequests", numRequests);
        modelAndView.addObject("status", accountService.getGroupStatus(account, group));
        return modelAndView;
    }

    @RequestMapping("/login/groupMember")
    public ModelAndView getGroupMembers(@RequestParam("groupId") long id) {
        Group group = groupService.get(id);
        Map<Account, String> accountAndPicture = new HashMap<>();
        for (Account account1 : groupService.getGroupMembers(group)) {
            accountAndPicture.put(account1, accountService.getStringPicture(account1));
        }
        ModelAndView modelAndView = new ModelAndView("groupMembers");
        modelAndView.addObject("membersAndPictures", accountAndPicture);
        return modelAndView;
    }

    @RequestMapping("/login/groupRequest")
    public ModelAndView getGroupRequest(@RequestParam("groupId") long id) {
        Group group = groupService.get(id);
        Map<Account, String> accountAndPicture = new HashMap<>();
        for (Account account1 : accountService.requestAccount(group)) {
            accountAndPicture.put(account1, accountService.getStringPicture(account1));
        }
        ModelAndView modelAndView = new ModelAndView("groupRequest");
        modelAndView.addObject("accountAndPicture", accountAndPicture);
        modelAndView.addObject("groupId", id);
        return modelAndView;
    }

    @RequestMapping("/login/joinGroup")
    public String joinGroup(@RequestParam("groupId") long id, HttpSession session) {
        Group group = groupService.get(id);
        Account account = (Account) session.getAttribute("accountSession");
        accountService.joinGroup(account, group);
        return "redirect: /login/group?groupId=" + id;
    }

    @RequestMapping("/login/acceptAccountGroup")
    public String acceptAccountGroup(@RequestParam("groupId") long groupId,
                                     @RequestParam("accountId") long accountId) {
        Account account = accountService.get(accountId);
        Group group = groupService.get(groupId);
        accountService.acceptInGroup(account, group);
        return "redirect: /login/groupRequest?groupId=" + groupId;
    }

    @RequestMapping("/login/rejectAccountGroup")
    public String rejectAccountGroup(@RequestParam("groupId") long groupId,
                                     @RequestParam("accountId") long accountId) {
        Account account = accountService.get(accountId);
        Group group = groupService.get(groupId);
        accountService.rejectGroup(account, group);
        return "redirect: /login/groupRequest?groupId=" + groupId;
    }

    @RequestMapping("/login/groups")
    public ModelAndView groupList(@ModelAttribute("accountSession") Account account) {
        logger.info("show account's groups");
        Map<Group, Boolean> groups = new HashMap<>();
        for (Group g : accountService.getAccountGroup(account)) {
            g.setStringPicture(groupService.getStringPicture(g));
            boolean isModerator = accountService.isModerator(account, g);
            groups.put(g, isModerator);
        }
        ModelAndView modelAndView = new ModelAndView("groupList");
        modelAndView.addObject("groups", groups);
        return modelAndView;
    }

    @RequestMapping("/login/search/account/{pageId}")
    public ModelAndView searchAccountByName(@RequestParam("search") String name,
                                            @PathVariable int pageId) {
        logger.info("search account with " + name + " parameter");
        Map<String, String> map = new HashMap<>();
        map.put("name", name);
        map.put("surname", name);
        List<Account> accountList = accountService.searchByName(map, pageId);
        Map<Account, String> accountAndPicture = new HashMap<>();
        for (Account account : accountList) {
            accountAndPicture.put(account, accountService.getStringPicture(account));
        }
        ModelAndView modelAndView = new ModelAndView("searchAccount");
        modelAndView.addObject("accountAndPicture", accountAndPicture);
        modelAndView.addObject("search", name);
        modelAndView.addObject("rows", accountService.getNumberOfRow(map));
        return modelAndView;
    }

    @RequestMapping("/login/search/group/{pageId}")
    public ModelAndView searchGroupByName(@RequestParam("search") String name,
                                          @PathVariable int pageId) {
        logger.info("search group with " + name + " parameter");
        Map<String, String> map = new HashMap<>();
        map.put("name", name);
        List<Group> groupList = groupService.searchByName(map, pageId);
        Map<Group, String> accountAndPicture = new HashMap<>();
        for (Group group : groupList) {
            accountAndPicture.put(group, groupService.getStringPicture(group));
        }
        ModelAndView modelAndView = new ModelAndView("searchGroup");
        modelAndView.addObject("groupAndPicture", accountAndPicture);
        modelAndView.addObject("search", name);
        modelAndView.addObject("rows", groupService.getNumberOfRow(map));
        return modelAndView;
    }

    @RequestMapping("/login/search/test/{pageId}")
    @ResponseBody
    public List<SearchElement> searchAccountTest(@RequestParam("search") String name,
                                                 @PathVariable int pageId) {
        logger.info("search account with " + name + " parameter (AJAX)");
        Map<String, String> map = new HashMap<>();
        map.put("name", name);
        map.put("surname", name);
        List<Account> accountList = accountService.searchByName(map, pageId);
        List<SearchElement> result = new ArrayList<>();
        for (Account account : accountList) {
            SearchElement searchElement = new SearchElement("account", account.getName() + " " + account.getSurname(), account.getId());
            searchElement.setStringPicture(accountService.getStringPicture(account));
            result.add(searchElement);
        }
        return result;
    }

    @RequestMapping("/login/search/testGroup/{pageId}")
    @ResponseBody
    public List<SearchElement> searchGroupTest(@RequestParam("search") String name,
                                               @PathVariable int pageId) {
        logger.info("search group with " + name + " parameter (AJAX)");
        Map<String, String> map = new HashMap<>();
        map.put("name", name);
        List<Group> groupList = groupService.searchByName(map, pageId);
        List<SearchElement> result = new ArrayList<>();
        for (Group group : groupList) {
            SearchElement searchElement = new SearchElement("account", group.getName(), group.getId());
            searchElement.setStringPicture(groupService.getStringPicture(group));
            result.add(searchElement);
        }
        return result;
    }

    @PostMapping("/login/updateGroup")
    public String updateGroup(@ModelAttribute Group group,
                              @RequestParam("file") CommonsMultipartFile file,
                              HttpSession session) throws IOException {
        logger.info("update group");
        group.setFounder((Account) session.getAttribute("accountSession"));
        if (file.isEmpty()) {
            group.setBytePicture(groupService.getBytePicture(group));
        } else {
            group.setBytePicture(file.getBytes());
        }
        groupService.updateEntity(group);
        return "redirect:/login/group?groupId=" + group.getId();
    }

    @RequestMapping("/login/search/autocomplete")
    @ResponseBody
    public List<SearchElement> getEntityElements(@RequestParam String name) {
        logger.info("return elements for autocomplete");
        List<SearchElement> resultList = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        map.put("name", name);
        map.put("surname", name);
        for (Account account : accountService.searchByName(map, 0)) {
            String title = account.getName() + " " + account.getSurname();
            resultList.add(new SearchElement("account", title, account.getId()));
        }
        map.remove("surname");
        for (Group group : groupService.searchByName(map, 0)) {
            resultList.add(new SearchElement("group", group.getName(), group.getId()));
        }
        return resultList;
    }

    @RequestMapping("/login/requestNum")
    @ResponseBody
    public int getRequestedFriends(@ModelAttribute("accountSession") Account account) {
        return accountService.requestForFriend(account).size();
    }

    @PostMapping("/login/wallMessage")
    @ResponseBody
    public SearchElement getAndSendWallMessage(@RequestBody AccountMessage accountMessage) {
        long senderId = accountMessage.getSenderId();
        long receiverId = accountMessage.getReceiverId();
        Account senderAccount = accountService.get(senderId);
        Account receiverAccount = accountService.get(receiverId);
        accountService.sendWallMessage(senderAccount, receiverAccount, accountMessage.getText());
        SearchElement searchElement = new SearchElement();
        searchElement.setLabel(accountMessage.getText());
        searchElement.setStringPicture(accountService.getStringPicture(senderAccount));
        searchElement.setId(senderId);
        searchElement.setType(accountMessage.getFrom());
        return searchElement;
    }

    @RequestMapping("/login/wallMessage/delete")
    @ResponseBody
    public boolean removeWallMessage(@RequestParam("id") long id) {
        accountService.deleteWallMessage(id);
        return true;
    }

    private static class SearchElement {
        private String type;
        private String label;
        private long id;
        private String stringPicture;

        SearchElement(String type, String label, long id) {
            this.type = type;
            this.label = label;
            this.id = id;
        }

        SearchElement() {
        }

        @Override
        public String toString() {
            return "SearchElement{" +
                    "type='" + type + '\'' +
                    ", name='" + label + '\'' +
                    ", id=" + id +
                    "stringpicture.size  " + (stringPicture == null ? 0 : stringPicture.length()) +
                    '}';
        }

        public String getStringPicture() {
            return stringPicture;
        }

        public void setStringPicture(String stringPicture) {
            this.stringPicture = stringPicture;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }
    }
}
