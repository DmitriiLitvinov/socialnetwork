package com.getjavajob.training.web1609.litvinovd.util;

import com.getjavajob.training.web1609.litvinovd.entity.Account;
import com.getjavajob.training.web1609.litvinovd.entity.Phone;
import com.getjavajob.training.web1609.litvinovd.entity.PhoneType;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ds.litvinov on 11.12.2016.
 */
public class AccountConverter implements Converter {

    public boolean canConvert(Class clazz) {
        return clazz.equals(Account.class);
    }

    public void marshal(Object object, HierarchicalStreamWriter writer, MarshallingContext context) {
        Account account = (Account) object;
        writer.startNode("name");
        writer.setValue(account.getName());
        writer.endNode();
        writer.startNode("surname");
        writer.setValue(account.getSurname());
        writer.endNode();
        writer.startNode("middleName");
        writer.setValue(account.getSurname());
        writer.endNode();
        writer.startNode("email");
        writer.setValue(account.getEmail());
        writer.endNode();
        writer.startNode("icq");
        writer.setValue(account.getIcq());
        writer.endNode();
        writer.startNode("skype");
        writer.setValue(account.getSkype());
        writer.endNode();
        for (Phone phone : account.getPhones()) {
            writer.startNode("phone");
            writer.startNode("num");
            writer.setValue(phone.getNum() + "");
            writer.endNode();
            writer.startNode("type");
            writer.setValue(phone.getType() + "");
            writer.endNode();
            writer.endNode();
        }
    }

    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        Account account = new Account();
        reader.moveDown();
        account.setName(reader.getValue());
        reader.moveUp();
        reader.moveDown();
        account.setSurname(reader.getValue());
        reader.moveUp();
        reader.moveDown();
        account.setMiddleName(reader.getValue());
        reader.moveUp();
        reader.moveDown();
        account.setEmail(reader.getValue());
        reader.moveUp();
        reader.moveDown();
        account.setIcq(reader.getValue());
        reader.moveUp();
        reader.moveDown();
        account.setSkype(reader.getValue());
        reader.moveUp();
        List<Phone> phoneList = new ArrayList<>();
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            Phone phone = new Phone();
            reader.moveDown();
            phone.setNum(Long.valueOf(reader.getValue()));
            reader.moveUp();
            reader.moveDown();
            phone.setType(PhoneType.valueOf(reader.getValue()));
            reader.moveUp();
            phoneList.add(phone);
            reader.moveUp();
        }

        account.setPhones(phoneList);
        return account;
    }
}
