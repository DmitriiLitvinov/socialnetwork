package com.getjavajob.training.web1609.litvinovd.ui;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by ds.litvinov on 24.01.2017.
 */
public class AuthenticationInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                             Object handler) throws IOException {
        HttpSession session = request.getSession(false);
        if (session != null && session.getAttribute("accountSession") != null) {
            return true;
        }
        response.sendRedirect(request.getContextPath() + "/");
        return false;
    }
}
