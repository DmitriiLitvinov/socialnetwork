package com.getjavajob.training.web1609.litvinovd.ui;

import com.getjavajob.training.web1609.litvinovd.entity.Account;
import com.getjavajob.training.web1609.litvinovd.entity.PhoneType;
import com.getjavajob.training.web1609.litvinovd.logic.AccountService;
import com.getjavajob.training.web1609.litvinovd.logic.util.AccountMessage;
import com.getjavajob.training.web1609.litvinovd.logic.util.AccountServiceException;
import com.getjavajob.training.web1609.litvinovd.util.AccountConverter;
import com.getjavajob.training.web1609.litvinovd.util.OutputMessage;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ds.litvinov on 24.01.2017.
 */
@Controller
@SessionAttributes("accountSession")
public class AccountController {

    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;
    private PhoneType[] types = new PhoneType[]{PhoneType.HOME, PhoneType.MOBILE, PhoneType.WORK};
    @Autowired
    private AccountService accountService;

    @RequestMapping("/login/addFriend")
    public String addFriend(HttpSession session,
                            @RequestParam("id") long id) {
        Account withWhom = accountService.get(id);
        Account account = (Account) session.getAttribute("accountSession");
        logger.info("WITH WHOM ADD FRIEND: " + withWhom);
        accountService.addFriend(account, withWhom);
        return "redirect: /login/accountPage?accountId=" + id;
    }

    @RequestMapping("/login/acceptFriend")
    public String acceptFriend(HttpSession session,
                               @RequestParam("id") long id) {
        Account withWhom = accountService.get(id);
        Account account = (Account) session.getAttribute("accountSession");
        accountService.acceptFriendship(account, withWhom);
        return "redirect: /login/getFriends";
    }

    @RequestMapping("/login/deleteFriend")
    public String deleteFriend(HttpSession session,
                               @RequestParam("id") long id) {
        Account removed = accountService.get(id);
        Account account = (Account) session.getAttribute("accountSession");
        accountService.deleteFriend(account, removed);
        return "redirect: /login/accountPage?accountId=" + id;
    }

    @RequestMapping("/login/getFriends")
    public ModelAndView getFriends(@ModelAttribute("accountSession") Account account) {
        Map<Account, String> accountAndPicture = new HashMap<>();
        for (Account account1 : accountService.getFriendList(account)) {
            accountAndPicture.put(account1, accountService.getStringPicture(account1));
        }
        ModelAndView modelAndView = new ModelAndView("showFriends");
        modelAndView.addObject("friendsAndPictures", accountAndPicture);
        return modelAndView;
    }

    @RequestMapping("/login/getRequests")
    public ModelAndView getRequestedFriend(HttpSession session) {
        Map<Account, String> accountAndPicture = new HashMap<>();
        Account account = (Account) session.getAttribute("accountSession");
        for (Account account1 : accountService.requestForFriend(account)) {
            accountAndPicture.put(account1, accountService.getStringPicture(account1));
        }
        ModelAndView modelAndView = new ModelAndView("requestFriends");
        modelAndView.addObject("friendsAndPictures", accountAndPicture);
        return modelAndView;
    }

    private String readFile(InputStream inputStream) {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader bf = new BufferedReader(new InputStreamReader(inputStream))) {
            String s;
            while ((s = bf.readLine()) != null) {
                sb.append(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    @RequestMapping("/login/loadToXml")
    public void loadToXml(@ModelAttribute("accountSession") Account account
            , HttpServletResponse response) throws IOException {
        logger.info("Convert account into xml file");
        XStream xStream = new XStream(new DomDriver());
        xStream.registerConverter(new AccountConverter());
        xStream.alias("account", Account.class);
        String xml = xStream.toXML(account);
        response.setContentType("application/xml");
        response.setHeader("Content-Disposition", "attachment; filename=account.xml");
        IOUtils.copy(new ByteArrayInputStream(xml.getBytes()), response.getOutputStream());
        response.flushBuffer();
        logger.info("Xml file is completed.");
    }

    @PostMapping("/login/loadAccountFromXml")
    public ModelAndView loadAccountFromXml(@RequestParam("file") CommonsMultipartFile file) throws IOException {
        logger.info("About to load xml file");
        XStream xStream = new XStream(new DomDriver());
        xStream.registerConverter(new AccountConverter());
        xStream.alias("account", Account.class);
        String stringFile = readFile(file.getInputStream());
        Account account = (Account) xStream.fromXML(stringFile);
        ModelAndView modelAndView = new ModelAndView("accountEdit");
        modelAndView.addObject("phoneTypes", types);
        modelAndView.addObject("account", account);
        logger.info("InputStream got from file set in session");
        return modelAndView;
    }

    @RequestMapping("/login/accountPage")
    public ModelAndView getAccountPage(@RequestParam("accountId") long id,
                                       @ModelAttribute("accountSession") Account accountSession) {
        logger.info("Load account page with id = " + id);
        Account account = accountService.get(id);
        ModelAndView modelAndView = new ModelAndView("accountPage");
        modelAndView.addObject("account", account);
        modelAndView.addObject("picture", accountService.getStringPicture(account));
        modelAndView.addObject("friendStatus", accountService.getFriendStatus(account, accountSession));
        modelAndView.addObject("messages", accountService.readMessage(accountSession, account));
        modelAndView.addObject("wallMessages", accountService.readWallMessage(account));
        logger.info("Ready to display account page");
        return modelAndView;
    }

    @RequestMapping("/login/accountPage/edit")
    public ModelAndView editAccount(@ModelAttribute("accountSession") Account account) {
        logger.info("method editAccount was called");
        ModelAndView modelAndView = new ModelAndView("accountEdit");
        modelAndView.addObject("phoneTypes", types);
        modelAndView.addObject("account", account);
        logger.info("About to display edit account page");
        return modelAndView;
    }

    @PostMapping("/loginSer")
    public ModelAndView login(@RequestParam("userEmail") String email,
                              @RequestParam("userPass") String pass,
                              @RequestParam(value = "remember", required = false) String remember, HttpSession session,
                              HttpServletResponse response) throws IOException, ServletException {
        logger.info("authentication");
        String password = DigestUtils.sha256Hex(pass);
        Account account;
        account = accountService.loginValidation(email, password);
        if (account != null) {
            session.setAttribute("accountSession", account);
            if (remember != null) {
                response.addCookie(new Cookie("userEmail", email));
                response.addCookie(new Cookie("userPass", password));
            }
            logger.info("email and password are valid");
            return new ModelAndView("redirect:/login/accountPage?accountId=" + account.getId());
        } else {
            logger.info("wrong email or password");
            return new ModelAndView("startJSP", "message", "You entered wrong email or password");
        }
    }

    @PostMapping("/login/accountPage/updatePage")
    public String updateAccount(@ModelAttribute Account account,
                                @RequestParam("password") String password, Model model) {
        logger.info("update page");
        account.setPassword(DigestUtils.sha256Hex(password));
        account.setBytePicture(accountService.getBytePicture(account));
        model.addAttribute("accountSession", account);
        accountService.updateEntity(account);

        return "redirect:/login/accountPage?accountId=" + account.getId();
    }

    @PostMapping("/signUp")
    public void registration(@ModelAttribute Account account,
                             @RequestParam("password") String password,
                             HttpServletResponse response) throws IOException, AccountServiceException {
        logger.info("sign up new account");
        String pass = DigestUtils.sha256Hex(password);
        account.setPassword(pass);
        DataInputStream stream = new DataInputStream(this.getClass().getClassLoader().
                getResourceAsStream("default_image.png"));
        account.setBytePicture(IOUtils.toByteArray(stream));
        stream.close();
        response.addCookie(new Cookie("userEmail", account.getEmail()));
        response.addCookie(new Cookie("userPass", account.getPassword()));
        accountService.createAccount(account);
        response.sendRedirect("login/accountPage?accountId=" + account.getId());
    }

    @RequestMapping({"/index", "/"})
    public ModelAndView goToAccountPage(HttpServletRequest request, HttpSession session) {
        logger.info("home page");
        Cookie[] cookies;
        Account account;
        if (session != null && (account = (Account) session.getAttribute("accountSession")) != null) {
            long id = account.getId();
            return new ModelAndView("redirect:/login/accountPage?accountId=" + id);
        } else if ((cookies = request.getCookies()) != null && cookies.length > 1) {
            String userEmail = null;
            String password = null;
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("userEmail")) {
                    userEmail = cookie.getValue();
                } else if (cookie.getName().equals("userPass")) {
                    password = cookie.getValue();
                }
            }
            if (userEmail != null && password != null) {
                if ((account = accountService.loginValidation(userEmail, password)) != null) {
                    session = request.getSession();
                    session.setAttribute("accountSession", account);
                    return new ModelAndView("redirect:/login/accountPage?accountId=" + account.getId());
                }
            }
        }
        return new ModelAndView("startJSP");
    }

    @RequestMapping("/logout")
    public String logout(HttpSession session, HttpServletResponse response, Model model) {
        logger.info("logout method");
        Cookie ck1 = new Cookie("userEmail", "");
        Cookie ck2 = new Cookie("userPass", "");
        ck1.setMaxAge(0);
        ck2.setMaxAge(0);
        response.addCookie(ck1);
        response.addCookie(ck2);
        session.invalidate();
        model.asMap().clear();
        return "redirect:/";
    }

    @PostMapping("/login/upload")
    public String uploadPicture(@RequestParam("file") CommonsMultipartFile file,
                                @ModelAttribute("accountSession") Account account) throws IOException {
        logger.info("update picture");
        accountService.loadPicture(account, file.getBytes());
        return "redirect:/login/accountPage?accountId=" + account.getId();
    }

    @MessageMapping("/chat")
    public void showMessage(AccountMessage message) {
        long senderId = message.getSenderId();
        long chatWithId = message.getReceiverId();
        Account account = accountService.get(senderId);
        Account chatWith = accountService.get(chatWithId);
        String time = new SimpleDateFormat("HH:mm").format(new Date());
        accountService.sendMessage(account, chatWith, message.getText());
        simpMessagingTemplate.convertAndSend("/topic/messages/" + senderId + "/" + chatWithId,
                new OutputMessage(message.getFrom(), message.getText(), time));
    }
}
