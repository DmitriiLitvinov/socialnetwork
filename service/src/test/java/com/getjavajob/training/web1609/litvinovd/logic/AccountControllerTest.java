package com.getjavajob.training.web1609.litvinovd.logic;

import com.getjavajob.training.web1609.litvinovd.dao.AccountDao;
import com.getjavajob.training.web1609.litvinovd.entity.Account;
import com.getjavajob.training.web1609.litvinovd.logic.util.AccountServiceException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by ds.litvinov on 25.11.2016.
 */
public class AccountControllerTest {

    private AccountDao accountDao;
    private Account account;
    private AccountService accountService;

    @Before
    public void init() {
        try {
            accountDao = mock(AccountDao.class);
            accountService = new AccountService(accountDao);
            AccountCreation accountCreation = new AccountCreation("accountScript.txt");
            account = accountCreation.createAccount();
            account.setId(1L);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void createAccountTest() throws AccountServiceException {
        when(accountDao.insert(account)).thenReturn(account);
        Account actual = accountService.createAccount(account);
        assertEquals(accountDao.insert(account), actual);
    }

    @Test
    public void editAccountTest() {
        accountService.updateEntity(account);
        verify(accountDao).update(account);
    }

    @Test
    public void deleteAccountTest() {
        accountService.deleteAccount(account);
        verify(accountDao).delete(account);
    }

    @Test
    public void addFriendTest() {
        accountService.addFriend(account, account);
        verify(accountDao).proposeFriendship(account, account, account);
    }

    @Test
    public void getFriendListTest() {
        accountService.getFriendList(account);
        verify(accountDao).getFriends(account);
    }
}
