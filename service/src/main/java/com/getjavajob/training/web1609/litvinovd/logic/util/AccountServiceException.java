package com.getjavajob.training.web1609.litvinovd.logic.util;

/**
 * Created by ds.litvinov on 01.12.2016.
 */
public class AccountServiceException extends Exception {
    public AccountServiceException(String str) {
        super(str);
    }
}
