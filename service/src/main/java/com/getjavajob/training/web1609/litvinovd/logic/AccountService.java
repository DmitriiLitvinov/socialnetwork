package com.getjavajob.training.web1609.litvinovd.logic;

import com.getjavajob.training.web1609.litvinovd.dao.AccountDao;
import com.getjavajob.training.web1609.litvinovd.entity.*;
import com.getjavajob.training.web1609.litvinovd.logic.util.AccountMessage;
import com.getjavajob.training.web1609.litvinovd.logic.util.AccountServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by ds.litvinov on 26.11.2016.
 */
@Service
public class AccountService extends AbstractService<Account> {

    @Autowired
    public AccountService(AccountDao accountDao) {
        super(accountDao);
    }

    private AccountDao getAccountDao() {
        return (AccountDao)dao;
    }
    
    @Transactional
    public Account createAccount(Account account) throws AccountServiceException {
            return getAccountDao().insert(account);
    }

    @Transactional
    public void deleteAccount(Account account) {
            getAccountDao().delete(account);

    }

    @Override
    @Transactional
    public void updateEntity(Account account) {
        List<Phone> phoneList = new ArrayList<>();
        for(Phone phone : account.getPhones()) {
            if(phone.getNum() != null) {
                phoneList.add(phone);
            }
        }
        account.setPhones(phoneList);
        dao.update(account);
    }

    @Transactional
    public void addFriend(Account account1, Account account2) {
        boolean lessId = account1.getId() < account2.getId();
        if(lessId) {
            getAccountDao().proposeFriendship(account1, account2, account2);
        } else {
            getAccountDao().proposeFriendship(account2, account1, account2);
        }
    }

    @Transactional
    public List<Account> getFriendList(Account account) {
            return getAccountDao().getFriends(account);
    }

    @Transactional
    public List<Account> requestForFriend(Account account) {
        return getAccountDao().requestForFriend(account);
    }

    @Transactional
    public String getFriendStatus(Account account1, Account account2) {
        boolean lessId = account1.getId() < account2.getId();
        if(lessId) {
            return getAccountDao().getFriendStatus(account1, account2);
        } else {
            return getAccountDao().getFriendStatus(account2, account1);
        }
    }

    @Transactional
    public void acceptFriendship(Account account1, Account account2) {
        boolean lessId = account1.getId() < account2.getId();
        if(lessId) {
            getAccountDao().acceptFriendship(account1, account2);
        } else {
            getAccountDao().acceptFriendship(account2, account1);
        }
    }

    @Transactional
    public void deleteFriend(Account account1, Account account2) {
        if(idIsLess(account1, account2)) {
            getAccountDao().deleteFriend(account1, account2);
        } else {
            getAccountDao().deleteFriend(account2, account1);
        }
    }

    @Transactional
    public Account loginValidation(String email, String password) {
            return getAccountDao().loginValidation(email, password);

    }

    @Transactional
    public boolean isModerator(Account account, Group group) {
           return getAccountDao().isModerator(account, group);

    }

    @Transactional
    public void joinGroup(Account account, Group group) {
            getAccountDao().joinGroup(account, group);

    }

    @Transactional
    public void acceptInGroup(Account ac1, Group group) {
        getAccountDao().acceptInGroup(ac1, group);

    }

    @Transactional
    public void rejectGroup(Account ac1, Group group) {
        getAccountDao().rejectGroup(ac1, group);
    }

    @Transactional
    public String getGroupStatus(Account ac1, Group group) {
        return getAccountDao().getGroupStatus(ac1, group);
    }

    @Transactional
    public List<Account> requestAccount(Group group) {
            return getAccountDao().requestAccount(group);

    }

    @Transactional
    public List<Group> getAccountGroup(Account account) {
            return getAccountDao().getGroups(account);

    }

    @Transactional
    public void loadPicture(Account account, byte[] bytes) throws IOException {
            getAccountDao().loadPicture(account, bytes);

    }

    @Transactional
    public String getStringPicture(Account account) {
            return Base64.getEncoder().encodeToString(getAccountDao().getBytePicture(account));

    }

    @Transactional
    public byte[] getBytePicture(Account account) {
           return getAccountDao().getBytePicture(account);

    }

    @Transactional
    public void sendMessage(Account sender, Account reciever, String text) {
        getAccountDao().sendMessage(sender, reciever, text);
    }

    @Transactional
    public Set<Message> readMessage(Account account1, Account account2) {
        return getAccountDao().readMessage(account1, account2);
    }

    @Transactional
    public void sendWallMessage(Account sender, Account receiver, String text) {
        getAccountDao().sendWallMessage(sender, receiver, text);
    }

    @Transactional
    public List<AccountMessage> readWallMessage(Account account1) {
        List<AccountMessage> result = new ArrayList<>();
        for(WallMessage wallMessage : getAccountDao().readWallMessage(account1)) {
            AccountMessage accountMessage = new AccountMessage();
            accountMessage.setFrom(wallMessage.getSender().getSurname());
            accountMessage.setDate(new SimpleDateFormat("YYYY/MM/dd").format(wallMessage.getDate()));
            accountMessage.setText(wallMessage.getText());
            accountMessage.setStringPicture(getStringPicture(wallMessage.getSender()));
            accountMessage.setSenderId(wallMessage.getSender().getId());
            accountMessage.setId(wallMessage.getId());
            result.add(accountMessage);
        }
        return result;
    }

    @Transactional
    public void deleteWallMessage(long id) {
        getAccountDao().deleteWallMessage(id);
    }

    private boolean idIsLess(Account account1, Account account2) {
        return account1.getId() < account2.getId();
    }

}
