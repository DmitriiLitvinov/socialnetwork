package com.getjavajob.training.web1609.litvinovd.logic;

import com.getjavajob.training.web1609.litvinovd.dao.*;
import com.getjavajob.training.web1609.litvinovd.entity.*;
import com.getjavajob.training.web1609.litvinovd.logic.util.AccountServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Base64;
import java.util.List;

/**
 * Created by ds.litvinov on 15.12.2016.
 */
@Service
public class GroupService extends AbstractService<Group> {
    private AccountDao accountDao;

    @Autowired
    public GroupService(GroupDao groupDao, AccountDao accountDao) {
        super(groupDao);
        this.accountDao = accountDao;
    }

    private GroupDao getGroupDao() {
        return (GroupDao) dao;
    }

    @Transactional
    public Group createGroup(Account account, Group group) throws AccountServiceException {
        Group grp = getGroupDao().insert(group);
        accountDao.joinGroup(account, grp);
        accountDao.acceptInGroup(account, grp);
        return grp;

    }

    @Transactional
    public String getStringPicture(Group group) {
        return Base64.getEncoder().encodeToString(getGroupDao().getBytePicture(group));
    }

    @Transactional
    public byte[] getBytePicture(Group group) {
        return getGroupDao().getBytePicture(group);
    }

    @Transactional
    public List<Account> getGroupMembers(Group group) {
        return getGroupDao().getAccounts(group);
    }

}
