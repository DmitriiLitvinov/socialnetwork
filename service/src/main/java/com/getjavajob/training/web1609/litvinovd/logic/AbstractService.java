package com.getjavajob.training.web1609.litvinovd.logic;

import com.getjavajob.training.web1609.litvinovd.dao.AbstractDao;
import com.getjavajob.training.web1609.litvinovd.entity.IdManipulation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by ds.litvinov on 04.01.2017.
 */
public class AbstractService<T extends IdManipulation> {
    protected AbstractDao<T> dao;

    @Value("${rowsForPage}")
    private int rowsForPage;

    public AbstractService(AbstractDao<T> abstractDao) {
        dao = abstractDao;
    }

    @Transactional
    public void updateEntity(T entity) {
        dao.update(entity);

    }

    @Transactional
    public List<T> getAll() {
        return dao.getAll();

    }

    @Transactional
    public T get(long id) {
        return dao.get(id);

    }

    @Transactional
    public List<T> searchByName(Map<String, String> mapName, int pageId) {
        if(pageId != 1) {
            pageId = (pageId - 1) * rowsForPage + 1;
        }
        return dao.searchByName(mapName, pageId, rowsForPage);
    }

    @Transactional
    public long getNumberOfRow(Map<String, String> mapName) {
        return dao.getNumberOfRow(mapName) / rowsForPage + 1;
    }
}
