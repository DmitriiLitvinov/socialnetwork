package com.getjavajob.training.web1609.litvinovd.logic;

import com.getjavajob.training.web1609.litvinovd.entity.Account;
import com.getjavajob.training.web1609.litvinovd.entity.Phone;
import com.getjavajob.training.web1609.litvinovd.entity.PhoneType;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by ds.litvinov on 23.11.2016.
 */
public class AccountCreation {

    private static final String[] fields = {"name", "surname", "middle name", "birth date (e.g. 1990-09-09)"
    , " home address", "work address", "email", "Icq", "skype", "extra info", "phones in " +
            "view numberPhone-umberType \nnumberTypes: home, work, mobile"};
    private String fileName;

    public AccountCreation(String name) {
        fileName = name;
    }

    public Account createAccount() throws FileNotFoundException {
        Scanner sc = new Scanner(AccountCreation.class.getClassLoader().getResourceAsStream(fileName));
        Account account = new Account();
        for (int i = 0; i < fields.length; i++) {
            String value = sc.nextLine();
            if(value.isEmpty()) {
                continue;
            }
            if(i == 10) {
                account.setPhones(readPhones(value, sc));
            } else {
                setAccountField(account, value, i);
            }
        }
        return account;
    }

    private void setAccountField(Account account, String value, int i) {
        switch (i) {
            case 0:
                account.setName(value);
                break;
            case 1:
                account.setSurname(value);
                break;
            case 2:
                account.setMiddleName(value);
                break;
            case 3:
                account.setBirthDate(convertToDate(value));
                break;
            case 4:
                account.setHomeAddress(value);
                break;
            case 5:
                account.setWorkAddress(value);
                break;
            case 6:
                account.setEmail(value);
                break;
            case 7:
                account.setIcq(value);
                break;
            case 8:
                account.setSkype(value);
                break;
            case 9:
                account.setExtraInfo(value);
                break;
        }
    }

    private  List<Phone> readPhones(String str, Scanner sc) {
        List<Phone> phoneList = new ArrayList<>();
        while (!str.isEmpty()) {
            Phone phone = createPhone(str);
            phoneList.add(phone);
            str = sc.nextLine();
        }
        return phoneList;
    }

    private  Phone createPhone(String str) {
        validPhone(str);
        Phone phone = new Phone();
        long number = Long.parseLong(str.split("-")[0]);
        String type = str.split("-")[1];
        phone.setNum(number);
        phone.setType(PhoneType.valueOf(type.toUpperCase()));
        return phone;
    }

    private  void validPhone(String strPhone) {
        if (!strPhone.matches("[0-9]*-((home)|(work)|(mobile))")) {
            throw new RuntimeException("Wrong phone/type enter");
        }
    }

    private GregorianCalendar convertToDate(String date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        format.setLenient(false);
        try {
            format.parse(date);
        } catch (ParseException e) {
            System.out.println("Wrong format of date");
        }
        String[] yMd = date.split("-");
        int year = Integer.parseInt(yMd[0]);
        int month = Integer.parseInt(yMd[1]);
        int day = Integer.parseInt(yMd[2]);
        return new GregorianCalendar(year, month, day);
    }
}
