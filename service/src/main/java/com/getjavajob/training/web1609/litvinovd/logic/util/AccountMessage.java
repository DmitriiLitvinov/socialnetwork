package com.getjavajob.training.web1609.litvinovd.logic.util;

/**
 * Created by ds.litvinov on 22.02.2017.
 */
public class AccountMessage {

    private long id;
    private String text;
    private String from;
    private long senderId;
    private long receiverId;
    private String stringPicture;
    private String date;

    public AccountMessage(){}

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public long getSenderId() {
        return senderId;
    }

    public void setSenderId(long senderId) {
        this.senderId = senderId;
    }

    public long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(long receiverId) {
        this.receiverId = receiverId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStringPicture() {
        return stringPicture;
    }

    public void setStringPicture(String stringPicture) {
        this.stringPicture = stringPicture;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
