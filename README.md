# Social Network

** Functionality: **

+ registration  
+ authentication  
+ display profile  
+ edit profile  
+ upload avatar  
+ create group  
+ join/reject member of group  
+ add/accept/delete friend  
+ private chat  
+ ajax notification of request for friend  
+ ajax leave/delete wall message  
+ ajax search with pagination  
+ user export to xml  

** Tools: **  
JDK 8, Spring 4, JPA 2 / Hibernate 4, XStream, jQuery 2, Twitter Bootstrap 3, JUnit 4, Mockito, Maven 3, Git / Bitbucket, Tomcat 8, MySQL, IntelliJIDEA 2016.  


** Notes: **  
SQL ddl is located in the `dao/src/test/resources/`

** Link to heroku: **  
Test account login/password: test@mail.ru / test  
http://checkoutsite.herokuapp.com

--  
**Литвинов Дмитрий**  
Тренинг getJavaJob,   
[http://www.getjavajob.com](http://www.getjavajob.com)